package exercise1_3;

import junit.framework.TestCase;

/**
 * Test case for the Automobile class using JUnit 4
 */
public class AutomobileTest extends TestCase {
    /**
     * this is testing for Constructor
     */
    public void testConstructorAutomobile() {
        new Automobile("Honda Civic", 20000, 30.0, false);
        new Automobile("Toyota Camry", 25000, 35.0, false);
        new Automobile("Ford Mustang", 30000, 25.0, false);
    }

    /**
     * this is testing for model function
     */
    public void testModel() {
        assertEquals("Honda Civic", new Automobile("Honda Civic", 20000, 30.0, false).getModel());
        assertEquals("Toyota Camry", new Automobile("Toyota Camry", 25000, 35.0, false).getModel());
        assertEquals("Ford Mustang", new Automobile("Ford Mustang", 30000, 25.0, false).getModel());
    }

    /**
     * this is testing for price function
     */
    public void testPrice() {
        assertEquals(20000, new Automobile("Honda Civic", 20000, 30.0, false).getPrice());
        assertEquals(25000, new Automobile("Toyota Camry", 25000, 35.0, false).getPrice());
        assertEquals(30000, new Automobile("Ford Mustang", 30000, 25.0, false).getPrice());
    }

    /**
     * this is testing for mileage function
     */
    public void testMileage() {
        assertEquals(30.0, new Automobile("Honda Civic", 20000, 30.0, false).getMileage(), 0.01);
        assertEquals(35.0, new Automobile("Toyota Camry", 25000, 35.0, false).getMileage(), 0.01);
        assertEquals(25.0, new Automobile("Ford Mustang", 30000, 25.0, false).getMileage(), 0.01);
    }

    /**
     * this is testing for used function
     */
    public void testUsed() {
        assertFalse(new Automobile("Honda Civic", 20000, 30.0, false).isUsed());
        assertFalse(new Automobile("Toyota Camry", 25000, 35.0, false).isUsed());
        assertFalse(new Automobile("Ford Mustang", 30000, 25.0, false).isUsed());
    }
}
