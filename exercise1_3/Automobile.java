package exercise1_3;

/**
 * Automobile class to represent an automobile
 */
public class Automobile {
    private String model;
    private int price; // in dollars
    private double mileage; // in miles per gallon
    private boolean used;

    /**
     * Constructor for the Automobile class
     * @param model Model of the automobile
     * @param price Price of the automobile in dollars
     * @param mileage Mileage of the automobile in miles per gallon
     * @param used Whether the automobile is used or not
     */
    public Automobile(String model, int price, double mileage, boolean used) {
        this.model = model;
        this.price = price;
        this.mileage = mileage;
        this.used = used;
    }

    /**
     * Returns the model of the automobile
     * @return Model of the automobile
     */
    public String getModel() {
        return model;
    }

    /**
     * Returns the price of the automobile in dollars
     * @return Price of the automobile in dollars
     */
    public int getPrice() {
        return price;
    }

    /**
     * Returns the mileage of the automobile in miles per gallon
     * @return Mileage of the automobile in miles per gallon
     */
    public double getMileage() {
        return mileage;
    }

    /**
     * Returns whether the automobile is used or not
     * @return Whether the automobile is used or not
     */
    public boolean isUsed() {
        return used;
    }
}