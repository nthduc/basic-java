package exercise1_7;

import junit.framework.TestCase;

/**
 * Test case for WeatherRecord class using JUnit 4.
 */
public class WeatherRecordTest extends TestCase {

	/**
	* This is testing for Constructor and getter methods for WeatherRecord class and its inner classes.
	*/
	public void testConstructorAndGetters() {

		// Create an instance of a Date using its constructor.
		Date d1 = new Date(1, 1, 2000);

		// Test that getter methods for Date class return correct values.
		assertEquals(1, d1.getDay());
		assertEquals(1, d1.getMonth());
		assertEquals(2000, d1.getYear());

		// Create an instance of a TemperatureRange using its constructor.
		TemperatureRange tr1 = new TemperatureRange(10, 20);

		// Test that getter methods for TemperatureRange class return correct values.
		assertEquals(10, tr1.getLow());
		assertEquals(20, tr1.getHigh());

		// Create another instance of a TemperatureRange using its constructor.
		TemperatureRange tr2 = new TemperatureRange(15, 25);

		// Test that getter methods for TemperatureRange class return correct values.
		assertEquals(15, tr2.getLow());
		assertEquals(25, tr2.getHigh());

		// Create another instance of a TemperatureRange using its constructor.
		TemperatureRange tr3 = new TemperatureRange(20, 30);

		// Test that getter methods for TemperatureRange class return correct values.
		assertEquals(20, tr3.getLow());
		assertEquals(30, tr3.getHigh());

		// Create an instance of a WeatherRecord using its constructor.
		WeatherRecord wr1 = new WeatherRecord(d1, tr1, tr2, tr3, 0.5);

		// Test that getter methods for WeatherRecord class return correct values.
		assertSame(d1, wr1.getDate());
		assertSame(tr1, wr1.getToday());
		assertSame(tr2, wr1.getNormal());
		assertSame(tr3, wr1.getRecord());
		assertEquals(0.5, wr1.getPrecipitation(), 0.01);
		
		 // Test that getter methods for returned Date object return correct values.
		 assertEquals(1, wr1.getDate().getDay());
		 assertEquals(1, wr1.getDate().getMonth());
		 assertEquals(2000, wr1.getDate().getYear());
		 
		 // Test that getter methods for returned TemperatureRange objects return correct values.
		 assertEquals(10, wr1.getToday().getLow());
		 assertEquals(20, wr1.getToday().getHigh());
		 assertEquals(15, wr1.getNormal().getLow());
		 assertEquals(25, wr1.getNormal().getHigh());
		 assertEquals(20, wr1.getRecord().getLow());
		 assertEquals(30, wr1.getRecord().getHigh());
		 
		  // Create another instance of a Date using its constructor.
		  Date d2 = new Date(2, 2, 2002);

		  // Test that getter methods for Date class return correct values.
		  assertEquals(2, d2.getDay());
		  assertEquals(2, d2.getMonth());
		  assertEquals(2002, d2.getYear());

		  // Create another instance of a TemperatureRange using its constructor.
		  TemperatureRange tr4 = new TemperatureRange(30, 40);

		  // Test that getter methods for TemperatureRange class return correct values.
		  assertEquals(30, tr4.getLow());
		  assertEquals(40, tr4.getHigh());

		  // Create another instance of a TemperatureRange using its constructor.
		  TemperatureRange tr5 = new TemperatureRange(35, 45);

		  // Test that getter methods for TemperatureRange class return correct values.
		  assertEquals(35, tr5.getLow());
		  assertEquals(45, tr5.getHigh());

		  // Create another instance of a TemperatureRange using its constructor.
		  TemperatureRange tr6 = new TemperatureRange(40, 50);

		  // Test that getter methods for TemperatureRange class return correct values.
		  assertEquals(40, tr6.getLow());
		  assertEquals(50, tr6.getHigh());

		  // Create another instance of a WeatherRecord using its constructor.
		  WeatherRecord wr2 = new WeatherRecord(d2, tr4, tr5, tr6, 0.6);

		  // Test that getter methods for WeatherRecord class return correct values.
		  assertSame(d2, wr2.getDate());
		  assertSame(tr4, wr2.getToday());
		  assertSame(tr5, wr2.getNormal());
		  assertSame(tr6, wr2.getRecord());
		  assertEquals(0.6, wr2.getPrecipitation(), 0.01);
		  
		   // Test that getter methods for returned Date object return correct values.
		   assertEquals(2, wr2.getDate().getDay());
		   assertEquals(2, wr2.getDate().getMonth());
		   assertEquals(2002, wr2.getDate().getYear());

		   // Test that getter methods for returned TemperatureRange objects return correct values.
		   assertEquals(30, wr2.getToday().getLow());
		   assertEquals(40, wr2.getToday().getHigh());
		   assertEquals(35, wr2.getNormal().getLow());
		   assertEquals(45, wr2.getNormal().getHigh());
		   assertEquals(40, wr2.getRecord().getLow());
		   assertEquals(50, wr2.getRecord().getHigh()); 
    }
}