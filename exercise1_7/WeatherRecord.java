package exercise1_7;

/**
 * WeatherRecord class to represent a weather record
 */
public class WeatherRecord {
    private Date d;
    private TemperatureRange today;
    private TemperatureRange normal;
    private TemperatureRange record;
    private double precipitation;

    /**
     * Constructor for the WeatherRecord class
     * @param d Date of the weather record
     * @param today Today's temperature range
     * @param normal Normal temperature range for the date
     * @param record Record temperature range for the date
     * @param precipitation Precipitation for the date
     */
    public WeatherRecord(Date d, TemperatureRange today, TemperatureRange normal, TemperatureRange record, double precipitation) {
        this.d = d;
        this.today = today;
        this.normal = normal;
        this.record = record;
        this.precipitation = precipitation;
    }

    /**
     * Returns the date of the weather record
     * @return Date of the weather record
     */
    public Date getDate() {
        return d;
    }

    /**
     * Returns today's temperature range
     * @return Today's temperature range
     */
    public TemperatureRange getToday() {
        return today;
    }

    /**
     * Returns normal temperature range for the date
     * @return Normal temperature range for the date
     */
    public TemperatureRange getNormal() {
        return normal;
    }

    /**
     * Returns record temperature range for the date
     * @return Record temperature range for the date
     */
    public TemperatureRange getRecord() {
        return record;
    }

    /**
     * Returns precipitation for the date
     * @return Precipitation for the date
     */
    public double getPrecipitation() {
        return precipitation;
    }
}
