package exercise1_7;

/**
 * TemperatureRange class to represent a temperature range
 */
public class TemperatureRange {
    private int low;
    private int high;

    /**
     * Constructor for the TemperatureRange class
     * @param low Low temperature of the range
     * @param high High temperature of the range
     */
    public TemperatureRange(int low, int high) {
        this.low = low;
        this.high = high;
    }

    /**
     * Returns the low temperature of the range
     * @return Low temperature of the range
     */
    public int getLow() {
        return low;
    }

    /**
     * Returns the high temperature of the range
     * @return High temperature of the range
     */
    public int getHigh() {
        return high;
    }
}