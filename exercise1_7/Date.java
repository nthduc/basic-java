package exercise1_7;

/**
 * Date class to represent a date
 */
public class Date {
    private int day;
    private int month;
    private int year;

    /**
     * Constructor for the Date class
     * @param day Day of the date
     * @param month Month of the date
     * @param year Year of the date
     */
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    /**
     * Returns the day of the date
     * @return Day of the date
     */
    public int getDay() {
        return day;
    }

    /**
     * Returns the month of the date
     * @return Month of the date
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns the year of the date
     * @return Year of the date
     */
    public int getYear() {
        return year;
    }
}
