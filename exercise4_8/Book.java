package exercise4_8;

public abstract class Book {
	protected String title;
	protected String author;
	protected double price;
	protected int year;

	public Book(String title, String author, double price, int year) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.year = year;
	}

	// tinh gia sau khi giam
	public abstract double salePrice();

	// kiem tra gia cua 1 quyen sach co re hon 1 vai quyen sach khac hay khong
	public boolean cheaperThan(Book that) {
		return this.price < that.price;
	}

	// kiem tra 2 quyen sach co cung tac gia hay khong
	public boolean sameAuthor(Book that) {
		return this.author.equals(that.author);
	}
}
