package exercise4_8;

public class SaleBook extends Book {

	public SaleBook(String title, String author, double price, int year) {
		super(title, author, price, year);
	}

	// tinh gia sau khi giam
	@Override
	public double salePrice() {
		return this.price * 0.5;
	}

}
