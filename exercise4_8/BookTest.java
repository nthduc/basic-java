package exercise4_8;

import junit.framework.TestCase;

public class BookTest extends TestCase {
	Book hardcover = new HardCoverBook("Truyen Kieu", "Nguyen Du", 30.0, 1999);
	Book salebook = new SaleBook("Thanh Giong", "Nguyen Trai", 20.0, 2002);
	Book paperback = new PaperBacks("Trinh Tham", "Nguyen Du", 100.0, 1945);

	public void testSaleBook() {
		assertEquals(hardcover.salePrice(), 24.0);
		assertEquals(salebook.salePrice(), 10.0);
		assertEquals(paperback.salePrice(), 100.0);
	}

	public void testCheaperThan() {
		assertTrue(hardcover.cheaperThan(paperback));
		assertTrue(salebook.cheaperThan(paperback));
		assertFalse(paperback.cheaperThan(salebook));
	}

	public void testSameAuthor() {
		assertTrue(hardcover.sameAuthor(paperback));
		assertFalse(salebook.sameAuthor(paperback));
		assertFalse(hardcover.sameAuthor(salebook));
	}
}
