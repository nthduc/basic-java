package baigiang5_2_inven1;

/**
 * Class representing a toy in an inventory.
 */
public class Toy {
    private String name;
    private double price;
    private int available;

    /**
     * Constructor for the Toy class. Initializes the name, price, and availability of the toy.
     * @param name The name of the toy.
     * @param price The price of the toy.
     * @param available The number of available units of the toy.
     */
    public Toy(String name, double price, int available) {
        this.name = name;
        this.price = price;
        this.available = available;
    }

    /**
     * Checks if this toy has the given name.
     * @param name The name to check against this toy's name.
     * @return Returns true if this toy has the given name, false otherwise.
     */
    public boolean isName(String name) {
        return this.name.equals(name);
    }
    
    public Toy copyWithRaisePrice(double rate) {

    	return new Toy(this.name,
    	this.price * (1 + rate), this.available);

    	}
    
    public void setNewPrice(double rate) {

    	this.price = this.price * (1 + rate);

    	}
    
    public boolean equals(Object obj) {

    	if (obj == null || !(obj instanceof Toy))

    	return false;

    	else {

    	Toy that = (Toy) obj;

    	return this.name.equals(that.name) &&

    	this.price == that.price &&

    	this.available == that.available;

    	}

    	}
}
