package baigiang5_2_inven1;

/**
 * Interface for an inventory.
 */
public interface Inventory {
    /**
     * Checks if the inventory contains a toy with the given name.
     * @param name The name of the toy to search for.
     * @return Returns true if the inventory contains a toy with the given name, false otherwise.
     */
    boolean contain(String name);
    public int howMany();
    public Inventory raisePrice(double rate);
    public void raisePriceMutable(double rate);
}