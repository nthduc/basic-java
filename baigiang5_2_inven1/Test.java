package baigiang5_2_inven1;
import junit.framework.TestCase;


public class Test extends TestCase {
    // Test the constructor and contain method of the ConsInventory class
    public void testConsInventory() {
        // Create some toys
        Toy t1 = new Toy("doll", 17.95, 5);
        Toy t2 = new Toy("robot", 22.05, 3);
        Toy t3 = new Toy("gun", 15.0, 4);

        // Create an empty inventory
        Inventory empty = new MTInventory();

        // Create an inventory with one toy
        Inventory i1 = new ConsInventory(t1, empty);

        // Check if the inventory contains the first toy
        assertTrue(i1.contain("doll"));

        // Check if the inventory does not contain a toy that is not in it
        assertFalse(i1.contain("robot"));

        // Create an inventory with two toys
        Inventory i2 = new ConsInventory(t2, i1);

        // Check if the inventory contains both toys
        assertTrue(i2.contain("doll"));
        assertTrue(i2.contain("robot"));

        // Check if the inventory does not contain a toy that is not in it
        assertFalse(i2.contain("gun"));
    }

    // Test the constructor and contain method of the MTInventory class
    public void testMTInventory() {
        // Create an empty inventory
        Inventory empty = new MTInventory();

        // Check if the empty inventory does not contain any toys
        assertFalse(empty.contain("doll"));
    }

    // Test the constructor and isName method of the Toy class
    public void testToy() {
        // Create a toy
        Toy t1 = new Toy("doll", 17.95, 5);

        // Check if the toy has the correct name
        assertTrue(t1.isName("doll"));

        // Check if the toy does not have a different name
        assertFalse(t1.isName("robot"));
    }
    
    public void testHowMany() {

    	Toy doll = new Toy("doll", 17.95, 5);

    	Toy robot = new Toy("robot", 22.05, 3);

    	Toy gun = new Toy ("gun", 15.0,4);

    	Inventory empty = new MTInventory();

    	Inventory i1 = new ConsInventory(doll, empty);

    	Inventory i2 = new ConsInventory(robot, i1);

    	Inventory all = new ConsInventory(doll,

    	new ConsInventory(robot,

    	new ConsInventory(gun, new MTInventory())));

    	assertEquals(0, empty.howMany());

    	assertEquals(1, i1.howMany());

    	assertEquals(2, i2.howMany());

    	assertEquals(3, all.howMany());

    	}
    
    
}
