package exercise2_1;

/**
 * The Employee class represents an employee in a company. It has attributes for
 * the employee's name and hours worked, and methods for calculating the
 * employee's wage, tax, net pay, and overtime status.
 */
public class Employee {
	String name;
	int hours;

	/**
	 * Constructor for the Employee class.
	 * 
	 * @param name  The name of the employee.
	 * @param hours The number of hours worked by the employee.
	 */
	public Employee(String name, int hours) {
		this.name = name;
		this.hours = hours;
	}

	/**
	 * Calculates the wage of the employee based on the number of hours worked.
	 * 
	 * @return The wage of the employee.
	 */
	int wage() {
		return this.hours * 12;
	}

	/**
	 * Calculates the tax to be paid by the employee.
	 * 
	 * @return The tax to be paid by the employee.
	 */
	double tax() {
		return this.wage() * 0.15;
	}

	/**
	 * Calculates the net pay of the employee.
	 * 
	 * @return The net pay of the employee.
	 */
	double netpay() {
		return this.wage() - this.tax();
	}

	/**
	 * Calculates the wage of the employee after a raise of 14 units is given.
	 * 
	 * @return The raised wage of the employee.
	 */
	int raisedWage() {
		return this.wage() + 14;
	}

	/**
	 * Checks if the employee has worked overtime.
	 * 
	 * @return True if the employee has worked overtime, false otherwise.
	 */
	boolean checkOverTime() {
		return this.hours > 100;
	}

	double taxWithRate() {

		double grossPay = this.wage();

		if (grossPay <= 240)

			return 0.0;

		if (grossPay <= 480)

			return grossPay * 0.15;

		return grossPay * 0.28;

	}

	double netpayWithRate() {

		return this.wage() - this.taxWithRate();

	}
}