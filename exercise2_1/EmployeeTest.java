package exercise2_1;

import junit.framework.TestCase;

public class EmployeeTest extends TestCase {
	public void testConstructor() {
		new Employee("ABC", 20);
		new Employee("XYZ", 10);
		new Employee("DEF", 18);

	}

	public void testWage() {
		assertEquals(new Employee("ABC", 20).wage(), 240);
		Employee e1 = new Employee("XYZ", 10);
		Employee e2 = new Employee("DEF", 18);
		assertEquals(e1.wage(), 120);
		assertEquals(e2.wage(), 216);
	}

	public void testTax() {
		Employee e = new Employee("ABC", 20);
		Employee e1 = new Employee("XYZ", 10);
		Employee e2 = new Employee("DEF", 18);
		assertEquals(e.tax(), 36.0, 0.001);
		assertEquals(e1.tax(), 18.0, 0.001);
		assertEquals(e2.tax(), 32.4, 0.001);
	}

	public void testNetpay() {
		Employee e = new Employee("ABC", 20);
		Employee e1 = new Employee("XYZ", 10);
		Employee e2 = new Employee("DEF", 18);
		assertEquals(e.netpay(), 204.0, 0.001);
		assertEquals(e1.netpay(), 102.0, 0.001);
		assertEquals(e2.netpay(), 183.6, 0.001);
	}

	public void testRaisedWage() {

		assertEquals(new Employee("ABC", 20).raisedWage(), 254, 0.001);

		Employee aEmployee1 = new Employee("XYZ", 10);

		Employee aEmployee2 = new Employee("DEF", 18);
		assertEquals(aEmployee1.raisedWage(), 134.0, 0.001);
		assertEquals(aEmployee2.raisedWage(), 230.0, 0.001);

	}

	public void testCheckOverTime() {

		assertFalse(new Employee("ABC", 20).checkOverTime());

		Employee aEmployee1 = new Employee("XYZ", 10);

		Employee aEmployee2 = new Employee("DEF", 18);
		assertFalse(aEmployee1.checkOverTime());
		assertFalse(aEmployee2.checkOverTime());

	}

	public void testTaxWithRate() {

		assertEquals(new Employee("ABC", 20).taxWithRate(), 0.0, 0.001);

		Employee aEmployee1 = new Employee("XYZ", 10);

		assertEquals(aEmployee1.taxWithRate(), 0.0, 0.001);

		Employee aEmployee2 = new Employee("Minh", 18);

		assertEquals(aEmployee2.taxWithRate(), 0.0, 0.001);

	}

	public void testNetpayWithRate() {

		assertEquals(new Employee("ABC", 20).netpayWithRate(),

		240.0, 0.001);
	}
}
