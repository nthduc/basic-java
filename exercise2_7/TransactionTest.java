package exercise2_7;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the Transaction class using JUnit 4
 */
public class TransactionTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorTransaction() {
        new Transaction("Alice", 1000.0, 1);
        new Transaction("Bob", 2000.0, 2);
        new Transaction("Charlie", 3000.0, 3);
    }

    /**
     * this is testing for interest function
     */
    @Test
    public void testInterest() {
        assertEquals(40.0, new Transaction("Alice", 1000.0, 1).interest(), 0.01);
        assertEquals(90.0, new Transaction("Bob", 2000.0, 2).interest(), 0.01);
        assertEquals(150.0, new Transaction("Charlie", 3000.0, 3).interest(), 0.01);
    }
}