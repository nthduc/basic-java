package exercise2_7;

public class Transaction {
    String customerName;
    double depositAmount;
    int maturity;

    /**
     * Constructor for the Transaction class
     * @param customerName Customer name as a string
     * @param depositAmount Deposit amount in dollars
     * @param maturity Maturity in years
     */
    public Transaction(String customerName, double depositAmount, int maturity) {
        this.customerName = customerName;
        this.depositAmount = depositAmount;
        this.maturity = maturity;
    }

    /**
     * Computes the interest earned on the deposit in a year
     * @return the interest earned in dollars
     */
    public double interest() {
        if (this.depositAmount <= 1000) {
            return this.depositAmount * 0.04;
        } else if (this.depositAmount <= 5000) {
            return this.depositAmount * 0.045;
        } else {
            return this.depositAmount * 0.05;
        }
    }
}
