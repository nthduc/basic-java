package exercise2_7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test case for the CreditCard class using JUnit 4
 */
public class CreditCardTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorCreditCard() {
        new CreditCard(1000.0);
        new CreditCard(2000.0);
        new CreditCard(3000.0);
    }

    /**
     * this is testing for payback function
     */
    @Test
    public void testPayback() {
        assertEquals(2.5, new CreditCard(1000.0).payback(), 0.01);
        assertEquals(7.5, new CreditCard(2000.0).payback(), 0.01);
        assertEquals(15.75, new CreditCard(3000.0).payback(), 0.01);
    }
}