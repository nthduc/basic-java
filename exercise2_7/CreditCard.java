package exercise2_7;

public class CreditCard {
    double chargeAmount;

    /**
     * Constructor for the CreditCard class
     * @param chargeAmount Charge amount in dollars
     */
    public CreditCard(double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    /**
     * Computes the payback amount on charges made over a year
     * @return the payback amount in dollars
     */
    public double payback() {
        double paybackAmount = 0.0;
        if (this.chargeAmount > 2500) {
            paybackAmount += (this.chargeAmount - 2500) * 0.01;
            this.chargeAmount = 2500;
        }
        if (this.chargeAmount > 1500) {
            paybackAmount += (this.chargeAmount - 1500) * 0.0075;
            this.chargeAmount = 1500;
        }
        if (this.chargeAmount > 500) {
            paybackAmount += (this.chargeAmount - 500) * 0.005;
            this.chargeAmount = 500;
        }
        paybackAmount += this.chargeAmount * 0.0025;
        return paybackAmount;
    }
}

