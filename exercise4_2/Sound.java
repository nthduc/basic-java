package exercise4_2;

/**
 * Represents a sound record in a gallery, with a playing time.
 */
public class Sound extends Record {
    private int playingTime;

    /**
     * Constructs a new Sound object with the given source file, size, and playing time.
     * @param sourceFile the source file of the sound
     * @param size the size of the sound in bytes
     * @param playingTime the playing time of the recording in seconds
     */
    public Sound(String sourceFile, int size, int playingTime) {
        super(sourceFile, size);
        this.playingTime = playingTime;
    }

    /**
     * Returns the playing time of this recording in seconds.
     * @return the playing time of this recording in seconds
     */
    public int getPlayingTime() {
        return playingTime;
    }
}