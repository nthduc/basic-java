package exercise4_2;

/**
 * Represents a record in a gallery, with a source file and a size.
 */
public abstract class Record {
    private String sourceFile;
    private int size;

    /**
     * Constructs a new Record object with the given source file and size.
     * @param sourceFile the source file of the record
     * @param size the size of the record in bytes
     */
    public Record(String sourceFile, int size) {
        this.sourceFile = sourceFile;
        this.size = size;
    }

    /**
     * Returns the source file of this record.
     * @return the source file of this record
     */
    public String getSourceFile() {
        return sourceFile;
    }

    /**
     * Returns the size of this record in bytes.
     * @return the size of this record in bytes
     */
    public int getSize() {
        return size;
    }
}