package exercise4_2;

/**
 * Represents a text record in a gallery, with a number of lines.
 */
public class Text extends Record {
    private int lines;

    /**
     * Constructs a new Text object with the given source file, size, and number of lines.
     * @param sourceFile the source file of the text
     * @param size the size of the text in bytes
     * @param lines the number of lines needed for visual representation
     */
    public Text(String sourceFile, int size, int lines) {
        super(sourceFile, size);
        this.lines = lines;
    }

    /**
     * Returns the number of lines needed for visual representation.
     * @return the number of lines needed for visual representation
     */
    public int getLines() {
        return lines;
    }
}