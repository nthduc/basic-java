package exercise4_2;

import junit.framework.TestCase;

public class RecordTest extends TestCase {
    public void testImageConstructor() {
        Image image = new Image("flower.gif", 57234, 100, 50, "medium");
        assertEquals("flower.gif", image.getSourceFile());
        assertEquals(57234, image.getSize());
        assertEquals(100, image.getWidth());
        assertEquals(50, image.getHeight());
        assertEquals("medium", image.getQuality());
    }

    public void testTextConstructor() {
        Text text = new Text("welcome.txt", 5312, 830);
        assertEquals("welcome.txt", text.getSourceFile());
        assertEquals(5312, text.getSize());
        assertEquals(830, text.getLines());
    }

    public void testSoundConstructor() {
        Sound sound = new Sound("theme.mp3", 40960, 200);
        assertEquals("theme.mp3", sound.getSourceFile());
        assertEquals(40960, sound.getSize());
        assertEquals(200, sound.getPlayingTime());
    }
}
