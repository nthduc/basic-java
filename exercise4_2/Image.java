package exercise4_2;

/**
 * Represents an image record in a gallery, with a width, height, and quality.
 */
public class Image extends Record {
    private int width;
    private int height;
    private String quality;

    /**
     * Constructs a new Image object with the given source file, size, width, height, and quality.
     * @param sourceFile the source file of the image
     * @param size the size of the image in bytes
     * @param width the width of the image in pixels
     * @param height the height of the image in pixels
     * @param quality the quality of the image
     */
    public Image(String sourceFile, int size, int width, int height, String quality) {
        super(sourceFile, size);
        this.width = width;
        this.height = height;
        this.quality = quality;
    }

    /**
     * Returns the width of this image in pixels.
     * @return the width of this image in pixels
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of this image in pixels.
     * @return the height of this image in pixels
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the quality of this image.
     * @return the quality of this image
     */
    public String getQuality() {
        return quality;
    }
}

