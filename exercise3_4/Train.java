package exercise3_4;

public class Train {
    private Schedule schedule;
    private Route route;
    private boolean local;

    /**
     * Constructor for the Train class. Initializes the schedule, route, and local status of the train.
     * @param schedule The schedule of the train.
     * @param route The route of the train.
     * @param local The local status of the train (true if it is a local train, false otherwise).
     */
    public Train(Schedule schedule, Route route, boolean local) {
        this.schedule = schedule;
        this.route = route;
        this.local = local;
    }

    public Schedule getSchedule() {
        return this.schedule;
    }

    public Route getRoute() {
        return this.route;
    }

	public boolean isLocal() {
		return this.local;
	}
}
