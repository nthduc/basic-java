package exercise3_4;

public class Route {
    private String origin;
    private String destination;

    /**
     * Constructor for the Route class. Initializes the origin and destination of the route.
     * @param origin The origin of the route.
     * @param destination The destination of the route.
     */
    public Route(String origin, String destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public String getDestination() {
        return this.destination;
    }

	public String getOrigin() {
		
		return this.origin;
	}
}