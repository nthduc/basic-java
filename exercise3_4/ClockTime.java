package exercise3_4;

public class ClockTime {
    private int hour;
    private int minute;

    /**
     * Constructor for the ClockTime class. Initializes the hour and minute of the clock time.
     * @param hour The hour of the clock time (0-23).
     * @param minute The minute of the clock time (0-59).
     */
    public ClockTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }
}