package exercise3_4;

import junit.framework.TestCase;

public class Test extends TestCase {
	public void testConstructor() {
		Route route = new Route("Seattle", "Portland");
		assertEquals("Seattle", route.getOrigin());
		assertEquals("Portland", route.getDestination());

		Schedule schedule = new Schedule(new ClockTime(9, 0), new ClockTime(12, 30));
		Train train = new Train(schedule, route, true);
		assertEquals(schedule, train.getSchedule());
		assertEquals(route, train.getRoute());
		assertTrue(train.isLocal());

		ClockTime departure = new ClockTime(9, 0);
		ClockTime arrive = new ClockTime(12, 30);
		assertEquals(departure, schedule.getDeparture());
		assertEquals(arrive, schedule.getArrive());

		ClockTime ct = new ClockTime(9, 30);
		assertEquals(9, ct.getHour());
		assertEquals(30, ct.getMinute());
	}

}
