package exercise3_4;

public class Schedule {
    private ClockTime departure;
    private ClockTime arrive;

    /**
     * Constructor for the Schedule class. Initializes the departure and arrival times of the schedule.
     * @param departure The departure time of the schedule.
     * @param arrive The arrival time of the schedule.
     */
    public Schedule(ClockTime departure, ClockTime arrive) {
        this.departure = departure;
        this.arrive = arrive;
    }

    public ClockTime getDeparture() {
        return this.departure;
    }

    public ClockTime getArrive() {
        return this.arrive;
    }
}
