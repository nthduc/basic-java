package exercise5_8;

import java.util.Objects;

public abstract class ABook {
	protected String title;
	protected String author;
	protected double price;
	protected int publicationYear;

	public ABook(String title, String author, double price, int publicationYear) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.publicationYear = publicationYear;
	}

	@Override
	public String toString() {
		return "ABook =" + title + ", author =" + author + ", price =" + price + ", publicationYear ="
				+ publicationYear;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ABook other = (ABook) obj;
		return Objects.equals(author, other.author)
				&& Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price)
				&& publicationYear == other.publicationYear && Objects.equals(title, other.title);
	}

	// kiem tra tac gia co trung voi tac gia cho truoc
	public boolean checkAuthor(String author2) {
		return this.author.equals(author2);
	}

	// so sanh ten
	protected boolean compareByTitle(ABook first) {
		return this.title.compareTo(first.title) <= 0;
	}
	
	// so sanh gia ban cua 2 quyen sach
	public boolean cheaperThan(ABook that) {
		return this.salePrice() < that.salePrice();
	}
	
	// kiem tra cung tac gia hay khong
	public boolean sameAuthor(ABook that) {
		return this.checkAuthor(that.author);
	}

	// gia ban
	public double salePrice() {
		return price;
	}
	
}
