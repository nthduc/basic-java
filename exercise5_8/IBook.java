package exercise5_8;

public interface IBook {
	
	// xac dinh xem tac gia co trung voi tac gia cho truoc
	public IBook thisAuthor(String author);
	
	//sap xep
	public IBook sortByTitle();
	
	//chen
	public IBook insertInTitleOrder(ABook first);
}
