package exercise5_8;

import junit.framework.TestCase;

public class IBookTest extends TestCase{
	ABook b1 = new Sale("tet la tet", "nguyen van ty", 20, 1930);
	ABook b2 = new Hardcover("mung xuan", "nguyen van teo", 10, 1940);
	ABook b3 = new Paperback("xuan yeu thuong", "nguyen van ty", 80, 1900);
	ABook b4 = new Sale("chuc tet", "nguyen van ty", 20, 1990);
	
	IBook empty = new MTBook();
	IBook i1 = new ConsBook(b1, empty);
	IBook i2 = new ConsBook(b2, i1);
	IBook i3 = new ConsBook(b3, i2);
	IBook i4 = new ConsBook(b4, i3);
	
	//test thisAuthor
	public void testThisAuthor() {
		assertEquals(i4.thisAuthor("nguyen van ty"), new ConsBook(b4, new ConsBook(b3, new ConsBook(b1, empty))));
		System.out.println("Nhung quyen sach cua tac gia Nguyen Van Ty: \n" + i4.thisAuthor("nguyen van ty"));
	}
	
	//test sortByTitle
	public void testSortByTitle() {
		assertEquals(i4.sortByTitle(), new ConsBook(b4, new ConsBook(b2, new ConsBook(b1, new ConsBook(b3, empty)))));
		System.out.println("Sap xep theo ten: \n" + i4.sortByTitle());
	}
	
	// test cheaperThan()
	public void testCheaperThan() {
		assertTrue(b2.cheaperThan(b1));
		assertFalse(b1.cheaperThan(b2));
		assertTrue(b4.cheaperThan(b3));
		assertFalse(b3.cheaperThan(b4));
	}
	
	// test sameAuthor()
		public void testSameAuthor() {
			assertTrue(b1.sameAuthor(b4));
			assertFalse(b1.sameAuthor(b2));
			assertTrue(b3.sameAuthor(b4));
			assertFalse(b3.sameAuthor(b2));
		}
}
