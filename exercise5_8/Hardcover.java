package exercise5_8;

public class Hardcover extends ABook{

	public Hardcover(String title, String author, double price, int publicationYear) {
		super(title, author, price, publicationYear);
	}
	
}
