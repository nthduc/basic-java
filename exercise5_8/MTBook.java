package exercise5_8;

public class MTBook implements IBook {

	// kiem tra tac gia co trung voi tac gia cho truoc
	@Override
	public IBook thisAuthor(String author) {
		return new MTBook();
	}

	// sap xep
	@Override
	public IBook sortByTitle() {
		return new MTBook();
	}

	// chen
	@Override
	public IBook insertInTitleOrder(ABook first) {
		return new ConsBook(first, new MTBook());
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (obj == null || !(obj instanceof MTBook))
			return false;
		return true;

	}

	@Override
	public String toString() {
		return "";
	}

}
