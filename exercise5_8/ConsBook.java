package exercise5_8;

import java.util.Objects;

public class ConsBook implements IBook {
	private ABook first;
	private IBook rest;

	public ConsBook(ABook first, IBook rest) {
		this.first = first;
		this.rest = rest;
	}

	@Override
	public String toString() {
		return first + "\n" + rest;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsBook other = (ConsBook) obj;
		return Objects.equals(first, other.first) && Objects.equals(rest, other.rest);
	}
	
// kiem tra tac gia co trung voi tac gia cho truoc
	@Override
	public IBook thisAuthor(String author) {
		if (this.first.checkAuthor(author)) {
			return new ConsBook(this.first, this.rest.thisAuthor(author));
		} else {
			return this.rest.thisAuthor(author);
		}
	}

	// sap xep
	@Override
	public IBook sortByTitle() {
		return this.rest.sortByTitle().insertInTitleOrder(this.first);
	}

	// chen 
	@Override
	public IBook insertInTitleOrder(ABook first) {
		if (first.compareByTitle(this.first))
			return new ConsBook(first, this);
		else
			return new ConsBook(this.first, this.rest.insertInTitleOrder(first));
	}

}
