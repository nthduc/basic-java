package exercise5_13;

public abstract class ARiver {
	private Location loc;
	private double lenght;

	public ARiver(Location loc, double lenght) {
		this.loc = loc;
		this.lenght = lenght;
	}

	public Location getLoc() {
		return loc;
	}

	public double getLenght() {
		return lenght;
	}

	@Override
	public String toString() {
		return "ARiver [loc=" + loc + ", lenght=" + lenght + "]";
	}
	
	// xac dinh doan song dai nhat
	public abstract double maxLenght();
	
	// xac dinh so luong hop luu cua he thong song
	protected abstract int getByNumberConFluence();
	
	public abstract ILocation getLocationSC();
}
