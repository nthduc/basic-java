package exercise5_13;

public class ConsLocation implements ILocation {
	private Location first;
	private ILocation left;
	private ILocation right;

	public ConsLocation(Location first, ILocation left, ILocation right) {
		this.first = first;
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return "ConsLocation " + first + ", left=" + left + ", right=" + right ;
	}

	public boolean equals(Object object) {
		if (object == null || !(object instanceof ConsLocation)) {
			return false;
		}
		ConsLocation that = (ConsLocation) object;
		if (this.first.equals(that.first) && this.left.equals(that.left) && this.right.equals(that.right)) {
			return true;
		}
		return false;
	}
}
