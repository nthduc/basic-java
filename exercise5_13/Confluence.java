package exercise5_13;

public class Confluence extends ARiver {
	private ARiver left;
	private ARiver right;

	public Confluence(Location loc, double lenght, ARiver left, ARiver right) {
		super(loc, lenght);
		this.left = left;
		this.right = right;
	}

	public ARiver getLeft() {
		return left;
	}

	public ARiver getRight() {
		return right;
	}

	@Override
	public String toString() {
		return "Confluence left=" + left + ", right=" + right;
	}

	// xac dinh doan song dai nhat
	@Override
	public double maxLenght() {
		if (this.getLenght() >= Math.max(this.left.getLenght(), this.right.getLenght())
				&& this.getLenght() >= Math.max(this.left.maxLenght(), this.right.maxLenght())) {
			return this.getLenght();
		}
		return Math.max(this.left.maxLenght(), this.right.maxLenght());
	}

	// xac dinh so luong hop luu cua he thong song
	@Override
	protected int getByNumberConFluence() {
		int result = 1;
		if (this.right != null) {
			result = result + this.right.getByNumberConFluence();
		}
		if (this.left != null) {
			result = result + this.right.getByNumberConFluence();
		}
		return result;
	}

	@Override
	public ILocation getLocationSC() {
		ILocation result = null;
		if (this.left != null) {
			result = new ConsLocation(this.getLoc(), this.left.getLocationSC(), this.right.getLocationSC());
		}
		return result;
	}

}
