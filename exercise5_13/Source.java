package exercise5_13;

public class Source extends ARiver {

	public Source(Location loc, double lenght) {
		super(loc, lenght);
	}

	@Override
	public String toString() {
		return "Source []";
	}

	// xac dinh doan song dai nhat
	@Override
	public double maxLenght() {
		return getLenght();
	}

	// xac dinh so luong hop luu cua he thong song
	@Override
	protected int getByNumberConFluence() {
		return 0;
	}

	@Override
	public ILocation getLocationSC() {
		return new ConsLocation(this.getLoc(), new MTLocation(), new MTLocation());
	}

}
