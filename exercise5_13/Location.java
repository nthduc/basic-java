package exercise5_13;

public class Location {
	private int x;
	private int y;
	private String name;

	public Location(int x, int y, String name) {
		this.x = x;
		this.y = y;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Location [x=" + x + ", y=" + y + ", name=" + name + "]";
	}

	public boolean equals(Object object) {
		if (object == null || !(object instanceof Location)) {
			return false;
		}
		Location that = (Location) object;
		if (this.x == that.x && this.y == that.y && this.name.equals(that.name)) {
			return true;
		}
		return false;
	}
}
