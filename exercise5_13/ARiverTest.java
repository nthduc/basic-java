package exercise5_13;

import junit.framework.TestCase;

public class ARiverTest extends TestCase {

	// khoi tao
	public void testConstructor() {
		ARiver s = new Source(new Location(1, 1, "s"), 120.0);
		ARiver t = new Source(new Location(1, 5, "t"), 50.0);
		ARiver u = new Source(new Location(3, 7, "u"), 100.0);

		ARiver b = new Confluence(new Location(3, 3, "b"), 60.0, s, t);
		ARiver a = new Confluence(new Location(5, 5, "a"), 30.0, b, u);

		Mouth m = new Mouth(new Location(7, 5, "m"), a);

		System.out.println(b);
		System.out.println(m);
	}

	// test maxlenght
	public void testMaxLenght() {
		ARiver s = new Source(new Location(1, 1, "s"), 120.0);
		ARiver t = new Source(new Location(1, 5, "t"), 50.0);
		ARiver u = new Source(new Location(3, 7, "u"), 100.0);

		ARiver b = new Confluence(new Location(3, 3, "b"), 60.0, s, t);
		ARiver a = new Confluence(new Location(5, 5, "a"), 30.0, b, u);

		Mouth m = new Mouth(new Location(7, 5, "m"), a);

		assertEquals(m.getMax(), 120, 0.001);
	}

	// test getConfluence
	public void testGetConfluence() {
		ARiver s = new Source(new Location(1, 1, "s"), 120.0);
		ARiver t = new Source(new Location(1, 5, "t"), 50.0);
		ARiver u1 = new Source(new Location(3, 7, "u1"), 100.0);
		ARiver u2 = new Source(new Location(7, 7, "u2"), 100.0);

		ARiver b = new Confluence(new Location(3, 3, "b"), 60.0, s, t);
		ARiver u = new Confluence(new Location(4, 4, "u"), 60.0, u1, u2);
		ARiver a = new Confluence(new Location(5, 5, "a"), 30.0, b, u);

		Mouth m = new Mouth(new Location(7, 5, "m"), a);

		assertEquals(m.getConFluence(), 3);
	}

	// test getAllLocation
	public void testGetAllLocation() {
		ARiver s = new Source(new Location(1, 1, "s"), 120.0);
		ARiver t = new Source(new Location(1, 5, "t"), 50.0);
		ARiver u1 = new Source(new Location(3, 7, "u1"), 100.0);
		ARiver u2 = new Source(new Location(7, 7, "u2"), 100.0);

		ARiver b = new Confluence(new Location(3, 3, "b"), 60.0, s, t);
		ARiver u = new Confluence(new Location(4, 4, "u"), 60.0, u1, u2);
		ARiver a = new Confluence(new Location(5, 5, "a"), 30.0, b, u);

		Mouth m = new Mouth(new Location(7, 5, "m"), a);

		assertEquals(s.getLocationSC(), new ConsLocation(new Location(1, 1, "s"), new MTLocation(), new MTLocation()));
		assertEquals(m.getAllLocation(), new ConsLocation(
				new Location(7, 5, "m"),
				new ConsLocation(
						new Location(5, 5, "a"),
						new ConsLocation(
								new Location(3, 3, "b"),
								new ConsLocation(new Location(1, 1, "s"), new MTLocation(), new MTLocation()),
								new ConsLocation(new Location(1, 5, "t"), new MTLocation(), new MTLocation())),
						new ConsLocation(new Location(4, 4, "u"),
								new ConsLocation(new Location(3, 7, "u1"), new MTLocation(), new MTLocation()),
								new ConsLocation(new Location(7, 7, "u2"), new MTLocation(), new MTLocation()))),
				new MTLocation()));

	}
}
