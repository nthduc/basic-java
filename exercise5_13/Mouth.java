package exercise5_13;

public class Mouth {
	private Location location;
	private ARiver river;

	public Mouth(Location location, ARiver river) {
		this.location = location;
		this.river = river;
	}

	@Override
	public String toString() {
		return "Mouth location=" + location + ", river=" + river;
	}

	// xac dinh doan song dai nhat
	public double getMax() {
		return this.river.maxLenght();
	}

	// xac dinh so luong hop luu cua he thong song
	public int getConFluence() {
		return this.river.getByNumberConFluence();
	}

	public ILocation getAllLocation() {
		return new ConsLocation(this.location, this.river.getLocationSC(), new MTLocation());
	}
}
