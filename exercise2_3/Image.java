package exercise2_3;

public class Image {
    int width;
    int height;

    public Image(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Determines whether the image's height is larger than its width
     * @return true if the image's height is larger than its width, false otherwise
     */
    public boolean isPortrait() {
        return this.height > this.width;
    }

    /**
     * Computes how many pixels the image contains
     * @return the number of pixels in the image
     */
    public int size() {
        return this.width * this.height;
    }

    /**
     * Determines whether this image contains more pixels than some other image
     * @param otherImage the other image to compare with
     * @return true if this image contains more pixels than the other image, false otherwise
     */
    public boolean isLarger(Image otherImage) {
        return this.size() > otherImage.size();
    }

    /**
     * Determines whether this image is the same as a given one
     * @param otherImage the other image to compare with
     * @return true if this image is the same as the given one, false otherwise
     */
    public boolean same(Image otherImage) {
        return this.width == otherImage.width && this.height == otherImage.height;
    }

    /**
     * Produces one of three strings, depending on the number of pixels in the image
     * @return "small" for images with 10,000 pixels or fewer; "medium" for images with between 10,001 and 1,000,000 pixels; "large" for images that are even larger than that.
     */
    public String sizeString() {
        int size = this.size();
        if (size <= 10000) {
            return "small";
        } else if (size <= 1000000) {
            return "medium";
        } else {
            return "large";
        }
    }
}

