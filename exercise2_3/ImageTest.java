package exercise2_3;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the Image class using JUnit 4
 */
public class ImageTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorImage() {
        new Image(100, 200);
        new Image(200, 300);
        new Image(300, 400);
    }

    /**
     * this is testing for isPortrait function
     */
    @Test
    public void testIsPortrait() {
        assertFalse(new Image(100, 200).isPortrait());
        assertTrue(new Image(200, 300).isPortrait());
        assertTrue(new Image(300, 400).isPortrait());
    }

    /**
     * this is testing for size function
     */
    @Test
    public void testSize() {
        assertEquals(20000, new Image(100, 200).size());
        assertEquals(60000, new Image(200, 300).size());
        assertEquals(120000, new Image(300, 400).size());
    }

    /**
     * this is testing for isLarger function
     */
    @Test
    public void testIsLarger() {
        assertFalse(new Image(100, 200).isLarger(new Image(200, 300)));
        assertFalse(new Image(200, 300).isLarger(new Image(300, 400)));
        assertTrue(new Image(300, 400).isLarger(new Image(100, 200)));
    }

    /**
     * this is testing for same function
     */
    @Test
    public void testSame() {
        assertFalse(new Image(100, 200).same(new Image(200, 300)));
        assertFalse(new Image(200, 300).same(new Image(300, 400)));
        assertTrue(new Image(300, 400).same(new Image(300, 400)));
    }

    /**
     * this is testing for sizeString function
     */
    @Test
    public void testSizeString() {
        assertEquals("small", new Image(100, 100).sizeString());
        assertEquals("medium", new Image(1000, 1000).sizeString());
        assertEquals("large", new Image(5000, 5000).sizeString());
    }
}
