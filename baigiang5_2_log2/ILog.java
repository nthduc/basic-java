package baigiang5_2_log2;

public interface ILog {

	// to compute the total number of miles

	// recorded in this log

	public double miles();
	public ILog getLogs(int month, int year);

	}