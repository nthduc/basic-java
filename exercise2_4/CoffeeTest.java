package exercise2_4;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the Coffee class using JUnit 4
 */
public class CoffeeTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorCoffee() {
        new Coffee("Arabica", 10.0, 1000.0);
        new Coffee("Robusta", 12.0, 2000.0);
        new Coffee("Liberica", 15.0, 3000.0);
    }

    /**
     * this is testing for cost function
     */
    @Test
    public void testCost() {
        assertEquals(10000.0, new Coffee("Arabica", 10.0, 1000.0).cost(), 0.01);
        assertEquals(21600.0, new Coffee("Robusta", 12.0, 2000.0).cost(), 0.01);
        assertEquals(33750.0, new Coffee("Liberica", 15.0, 3000.0).cost(), 0.01);
    }
}
