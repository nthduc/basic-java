package exercise2_4;

public class Coffee {
    String kind;
    double unitPrice;
    double weight;

    public Coffee(String kind, double unitPrice, double weight) {
        this.kind = kind;
        this.unitPrice = unitPrice;
        this.weight = weight;
    }

    /**
     * Computes the cost of selling bulk coffee at a specialty coffee seller
     * @return the cost of selling bulk coffee
     */
    public double cost() {
        double discount = 0.0;
        if (this.weight >= 5000 && this.weight < 20000) {
            discount = 0.1;
        } else if (this.weight >= 20000) {
            discount = 0.25;
        }
        return this.unitPrice * this.weight * (1 - discount);
    }
}
