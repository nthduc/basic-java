package exercise5_3;

public class ConsGrades implements IGrades {
	private GradeRecord first;
	private IGrades rest;

	public ConsGrades(GradeRecord first, IGrades rest) {
		this.first = first;
		this.rest = rest;
	}

	@Override
	public String toString() {
		return this.first.toString() + " \n" + this.rest.toString();
	}

}
