package exercise5_3;

import junit.framework.TestCase;

public class CourseTest extends TestCase {
	// create graderecord
	public void testScoreBoard() {
		Course c1 = new Course(111, "Lap trinh co ban", 4);
		Course c2 = new Course(222, "Lap trinh nang cao", 4);
		Course c3 = new Course(333, "Lap trinh web", 4);
		Course c4 = new Course(777, "Tieng anh B1", 1);
		IGrades grades = new ConsGrades(new GradeRecord(c1, 7.5), 
				new ConsGrades(new GradeRecord(c2, 8.0),
				new ConsGrades(new GradeRecord(c3, 7.0), 
				new ConsGrades(new GradeRecord(c4, 8.0), 
				new EmptyGrades()))));
		ScoreBoard student = new ScoreBoard("Nguyen Van A", "DH**DTC", grades);
		System.out.println(student);
	}
}
