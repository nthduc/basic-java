package exercise5_3;

public class ScoreBoard {
	private String name;
	private String classRoom;
	private IGrades loGrade;

	public ScoreBoard(String name, String classRoom, IGrades loGrade) {
		this.name = name;
		this.classRoom = classRoom;
		this.loGrade = loGrade;
	}

	@Override
	public String toString() {
		return this.name + " (" + this.classRoom + ") \n" + this.loGrade.toString();
	}
}
