package exercise5_11;

import junit.framework.TestCase;

public class RecordTest extends TestCase {
	GradeRecord g1 = new GradeRecord(new Course(111, "Lap trinh co ban", 3), 7.5);
	GradeRecord g2 = new GradeRecord(new Course(222, "Lap trinh mang", 4), 5.0);
	GradeRecord g3 = new GradeRecord(new Course(333, "Lap trinh web", 4), 7.0);
	GradeRecord g4 = new GradeRecord(new Course(444, "Phan tich thiet ke", 4), 8.0);

	ScoreBoard s1 = new ScoreBoard("Nguyen Van A", 2022, g1);
	ScoreBoard s2 = new ScoreBoard("Nguyen Van B", 2022, g2);
	ScoreBoard s3 = new ScoreBoard("Nguyen Thi C", 2022, g3);
	ScoreBoard s4 = new ScoreBoard("Nguyen Thi D", 2022, g4);

	IRecord empty = new MTRecord();
	IRecord c1 = new ConsRecord(s1, empty);
	IRecord c2 = new ConsRecord(s2, c1);
	IRecord c3 = new ConsRecord(s3, c2);
	IRecord all = new ConsRecord(s4, c3);

	public void testConstructor() {
		GradeRecord g1 = new GradeRecord(new Course(111, "Lap trinh co ban", 3), 7.5);
		GradeRecord g2 = new GradeRecord(new Course(222, "Lap trinh mang", 4), 5.0);
		GradeRecord g3 = new GradeRecord(new Course(333, "Lap trinh web", 4), 7.0);
		GradeRecord g4 = new GradeRecord(new Course(444, "Phan tich thiet ke", 4), 8.0);

		ScoreBoard s1 = new ScoreBoard("Nguyen Van A", 2022, g1);
		ScoreBoard s2 = new ScoreBoard("Nguyen Van B", 2022, g2);
		ScoreBoard s3 = new ScoreBoard("Nguyen Thi C", 2022, g3);
		ScoreBoard s4 = new ScoreBoard("Nguyen Thi D", 2022, g4);

		IRecord empty = new MTRecord();
		IRecord c1 = new ConsRecord(s1, empty);
		IRecord c2 = new ConsRecord(s2, c1);
		IRecord c3 = new ConsRecord(s3, c2);
		IRecord all = new ConsRecord(s4, c3);

		System.out.println("Bang diem:\n" + all);
		System.out.println("Tong so tin chi:\n" + all.howManyCredits());
		System.out.println("\nTong diem:\n" + all.getTotalScore());
		System.out.println("\nDiem trung binh:\n" + all.gradeAverage());
		System.out.println("\nDanh sach sinh vien co diem lon hon 7.0:\n" + all.listGreaterThan(7.0));
		;
	}
		
	public void testHowManyCredits() {
		assertEquals(all.howManyCredits(), 15);
	}

	public void testGradeAverage() {
		assertEquals(all.gradeAverage(),6.8333,0.001);

	}
	public void testLisGreaterThan() {
		assertEquals(all.listGreaterThan(7.0),new ConsRecord(s4, new ConsRecord(s1, empty)));
	}
}
