package exercise5_11;

public class GradeRecord {
	Course course;
	double grade;

	public GradeRecord(Course course, double grade) {
		this.course = course;
		this.grade = grade;
	}

	@Override
	public String toString() {
		return this.course.toString() + this.grade;

	}

	public int getCredit() {
		return this.course.getCredit();
	}

	public double kkk() {
		return this.course.ttt() * this.grade;
	}

	public boolean scoreGreater(double thatGrade) {
		return this.grade > thatGrade;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof GradeRecord))
			return false;
		else {
			GradeRecord that = (GradeRecord) obj;
			return this.course.equals(that.course) && this.grade == that.grade;
		}
	}
}
