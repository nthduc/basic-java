package exercise5_11;

public class ConsRecord implements IRecord {
	ScoreBoard first;
	IRecord rest;

	public ConsRecord(ScoreBoard first, IRecord rest) {
		this.first = first;
		this.rest = rest;
	}

	public String toString() {
		return this.first.toString() + "\n" + this.rest.toString();
	}

	@Override
	public int howManyCredits() {
		return this.first.getCredit() + this.rest.howManyCredits();
	}

	@Override
	public double getTotalScore() {
		// TODO Auto-generated method stub
		return (this.first.mmm() + this.rest.getTotalScore());
	}

	@Override
	public double gradeAverage() {
		// TODO Auto-generated method stub
		return this.getTotalScore() / this.howManyCredits();

	}

	@Override
	public IRecord listGreaterThan(double thatGrade) {
		if (this.first.scoreGreater(thatGrade))
			return new ConsRecord(this.first, this.rest.listGreaterThan(thatGrade));
		else {
			return this.rest.listGreaterThan(thatGrade);
		}
	}

	public boolean equals(Object obj) {
		if (null == obj || !(obj instanceof ConsRecord))
			return false;
		else {
			ConsRecord that = (ConsRecord) obj;
			return this.first.equals(that.first) && this.rest.equals(that.rest);
		}
	}

}