package exercise5_11;

public class Course {
	int IDSubject;
	String subjectName;
	int credits;

	public Course(int iDSubject, String subjectName, int credits) {
		IDSubject = iDSubject;
		this.subjectName = subjectName;
		this.credits = credits;
	}

	@Override
	public String toString() {
		return "Course [IDSubject: " + IDSubject + ", subjectName: " + subjectName + ", credits: " + credits + "]";
	}

	public int getCredit() {
		return this.credits;
	}

	public double ttt() {
		return (double) this.credits;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Course))
			return false;
		else {
			Course that = (Course) obj;
			return this.IDSubject == that.IDSubject && this.subjectName.equals(that.subjectName)
					&& this.credits == that.credits;
		}
	}
}
