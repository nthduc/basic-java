package exercise5_11;

public class ScoreBoard {
	String name;
	int schoolYear;
	GradeRecord gradeRecord;

	public ScoreBoard(String name, int schoolYear, GradeRecord gradeRecord) {
		this.name = name;
		this.schoolYear = schoolYear;
		this.gradeRecord = gradeRecord;
	}

	@Override
	public String toString() {
		return "[ScoreBoard] name: " + name + ", schoolYear: " + schoolYear + ", (gradeRecord): "
				+ gradeRecord.toString();
	}

	public int getCredit() {
		return this.gradeRecord.getCredit();
	}

	public double mmm() {
		return this.gradeRecord.kkk();
	}

	public boolean scoreGreater(double thatGrade) {
		return this.gradeRecord.scoreGreater(thatGrade);
	}

	public boolean equals(Object obj) {
		if (null == obj || !(obj instanceof ScoreBoard))
			return false;
		else {
			ScoreBoard that = (ScoreBoard) obj;
			return this.name.equals(that.name) && this.schoolYear == that.schoolYear
					&& this.gradeRecord.equals(that.gradeRecord);
		}
	}
}
