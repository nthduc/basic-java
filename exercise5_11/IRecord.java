package exercise5_11;

public interface IRecord {
	// so tin chi
	public int howManyCredits();

	// tong diem
	double getTotalScore();

	// diem trung binh
	public double gradeAverage();

	// danh sach co diem lon hon thatGrade
	public IRecord listGreaterThan(double thatGrade);

}
