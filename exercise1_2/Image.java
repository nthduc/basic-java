package exercise1_2;

/**
 * Image class to represent computer images
 */
public class Image {
    private int height; // pixels
    private int width; // pixels
    private String source; // file name
    private String quality; // informal

    /**
     * Constructor for the Image class
     * @param height Height of the image in pixels
     * @param width Width of the image in pixels
     * @param source File name of the image
     * @param quality Informal quality of the image
     */
    public Image(int height, int width, String source, String quality) {
        this.height = height;
        this.width = width;
        this.source = source;
        this.quality = quality;
    }

    /**
     * Returns the height of the image in pixels
     * @return Height of the image in pixels
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the width of the image in pixels
     * @return Width of the image in pixels
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the file name of the image
     * @return File name of the image
     */
    public String getSource() {
        return source;
    }

    /**
     * Returns the informal quality of the image
     * @return Informal quality of the image
     */
    public String getQuality() {
        return quality;
    }
}