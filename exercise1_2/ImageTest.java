package exercise1_2;

import junit.framework.TestCase;

/**
 * Test case for the Image class using JUnit 4
 */
public class ImageTest extends TestCase {
    /**
     * this is testing for Constructor
     */
    public void testConstructorImage() {
        new Image(5, 10, "small.gif", "low");
        new Image(120, 200, "med.gif", "low");
        new Image(1200, 1000, "large.gif", "high");
    }

    /**
     * this is testing for height function
     */
    public void testHeight() {
        assertEquals(5, new Image(5, 10, "small.gif", "low").getHeight());
        assertEquals(120, new Image(120, 200, "med.gif", "low").getHeight());
        assertEquals(1200, new Image(1200, 1000, "large.gif", "high").getHeight());
    }

    /**
     * this is testing for width function
     */
    public void testWidth() {
        assertEquals(10, new Image(5, 10, "small.gif", "low").getWidth());
        assertEquals(200, new Image(120, 200, "med.gif", "low").getWidth());
        assertEquals(1000, new Image(1200, 1000, "large.gif", "high").getWidth());
    }

    /**
     * this is testing for source function
     */
    public void testSource() {
        assertEquals("small.gif", new Image(5, 10, "small.gif", "low").getSource());
        assertEquals("med.gif", new Image(120, 200, "med.gif", "low").getSource());
        assertEquals("large.gif", new Image(1200, 1000, "large.gif", "high").getSource());
    }

    /**
     * this is testing for quality function
     */
    public void testQuality() {
        assertEquals("low", new Image(5, 10, "small.gif", "low").getQuality());
        assertEquals("low", new Image(120, 200, "med.gif", "low").getQuality());
        assertEquals("high", new Image(1200, 1000, "large.gif", "high").getQuality());
    }
}
