package exercise2_2;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the MovieShow class using JUnit 4
 */
public class MovieShowTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorMovieShow() {
        new MovieShow(10.0, 100.0, 1.0);
        new MovieShow(12.0, 120.0, 1.5);
        new MovieShow(15.0, 150.0, 2.0);
    }

    /**
     * this is testing for cost function
     */
    @Test
    public void testCost() {
        assertEquals(110.0, new MovieShow(10.0, 100.0, 1.0).cost(10), 0.01);
        assertEquals(135.0, new MovieShow(12.0, 120.0, 1.5).cost(10), 0.01);
        assertEquals(170.0, new MovieShow(15.0, 150.0, 2.0).cost(10), 0.01);
    }

    /**
     * this is testing for revenue function
     */
    @Test
    public void testRevenue() {
        assertEquals(100.0, new MovieShow(10.0, 100.0, 1.0).revenue(10), 0.01);
        assertEquals(120.0, new MovieShow(12.0, 120.0, 1.5).revenue(10), 0.01);
        assertEquals(150.0, new MovieShow(15.0, 150.0, 2.0).revenue(10), 0.01);
    }

    /**
     * this is testing for totalProfit function
     */
    @Test
    public void testTotalProfit() {
        assertEquals(-10.0, new MovieShow(10.0, 100.0, 1.0).totalProfit(10), 0.01);
        assertEquals(-15.0, new MovieShow(12.0, 120.0, 1.5).totalProfit(10), 0.01);
        assertEquals(-20.0, new MovieShow(15.0, 150.0, 2.0).totalProfit(10), 0.01);
    }
}
