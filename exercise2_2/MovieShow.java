package exercise2_2;

/**
 * MovieShow class to represent a movie show
 */
public class MovieShow {
    double ticketPrice;
    double costForPerformance;
    double costPerAttende;

    /**
     * Constructor for the MovieShow class
     * @param ticketPrice Price of a ticket in dollars
     * @param costForPerformance Cost for the performance in dollars
     * @param costPerAttende Cost per attendee in dollars
     */
    public MovieShow(double ticketPrice, double costForPerformance, double costPerAttende) {
        this.ticketPrice = ticketPrice;
        this.costForPerformance = costForPerformance;
        this.costPerAttende = costPerAttende;
    }

    /**
     * Calculates the total cost for a given number of attendees
     * @param numAttende Number of attendees
     * @return Total cost in dollars
     */
    double cost(int numAttende) {
        return this.costForPerformance + this.costPerAttende * numAttende;
    }

    /**
     * Calculates the total revenue for a given number of attendees
     * @param numAttendee Number of attendees
     * @return Total revenue in dollars
     */
    double revenue(int numAttendee) {
        return this.ticketPrice * numAttendee;
    }

    /**
     * Calculates the total profit for a given number of attendees
     * @param numAttende Number of attendees
     * @return Total profit in dollars
     */
    double totalProfit(int numAttende) {
        return this.revenue(numAttende) - this.cost(numAttende);
    }
}
