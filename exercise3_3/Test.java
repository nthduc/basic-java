package exercise3_3;

import junit.framework.TestCase;


public class Test extends TestCase {
    public void testDateConstructor() {
        Date d = new Date(1, 2, 2022);
        assertNotNull(d);
    }

    public void testTemperatureRangeConstructor() {
        TemperatureRange tr = new TemperatureRange(10, 20);
        assertNotNull(tr);
    }

    public void testWeatherRecordConstructor() {
        Date d = new Date(1, 2, 2022);
        TemperatureRange today = new TemperatureRange(10, 20);
        TemperatureRange normal = new TemperatureRange(5, 25);
        TemperatureRange record = new TemperatureRange(0, 30);
        double precipitation = 0.5;
        WeatherRecord wr = new WeatherRecord(d, today, normal, record, precipitation);
        assertNotNull(wr);
    }

    public void testWithin() {
        TemperatureRange tr1 = new TemperatureRange(10, 20);
        TemperatureRange tr2 = new TemperatureRange(5, 25);
        assertTrue(tr1.within(tr2));
    }

    public void testWithinRange() {
        Date d = new Date(1, 2, 2022);
        TemperatureRange today = new TemperatureRange(10, 20);
        TemperatureRange normal = new TemperatureRange(5, 25);
        TemperatureRange record = new TemperatureRange(0, 30);
        double precipitation = 0.5;
        WeatherRecord wr = new WeatherRecord(d, today, normal, record, precipitation);
        assertTrue(wr.withinRange());
    }

    public void testRainyDay() {
        Date d = new Date(1, 2, 2022);
        TemperatureRange today = new TemperatureRange(10, 20);
        TemperatureRange normal = new TemperatureRange(5, 25);
        TemperatureRange record = new TemperatureRange(0, 30);
        double precipitation = 0.5;
        WeatherRecord wr = new WeatherRecord(d, today, normal, record, precipitation);
        assertTrue(wr.rainyDay(0.4));
    }

    public void testRecordDay() {
        Date d = new Date(1, 2, 2022);
        TemperatureRange today = new TemperatureRange(10, 20);
        TemperatureRange normal = new TemperatureRange(5, 25);
        TemperatureRange record = new TemperatureRange(15, 18);
        double precipitation = 0.5;
        WeatherRecord wr = new WeatherRecord(d, today, normal, record, precipitation);
        assertTrue(wr.recordDay());
    }
}

