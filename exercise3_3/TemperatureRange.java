package exercise3_3;

public class TemperatureRange {
    private int low;
    private int high;

    /**
     * Constructor for the TemperatureRange class. Initializes the low and high temperatures of the range.
     * @param low The low temperature of the range.
     * @param high The high temperature of the range.
     */
    public TemperatureRange(int low, int high) {
        this.low = low;
        this.high = high;
    }

    /**
     * Checks if this temperature range is within another temperature range.
     * @param that The other temperature range to compare with.
     * @return Returns true if this temperature range is within the other temperature range, false otherwise.
     */
    public boolean within(TemperatureRange that) {
        return (this.low >= that.low) && (this.high <= that.high);
    }
}