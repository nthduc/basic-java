package exercise3_3;

public class WeatherRecord {
    private Date d;
    private TemperatureRange today;
    private TemperatureRange normal;
    private TemperatureRange record;
    private double precipitation;

    /**
     * Constructor for the WeatherRecord class. Initializes the date, today's temperature range, normal temperature range, record temperature range, and precipitation of the weather record.
     * @param d The date of the weather record.
     * @param today Today's temperature range.
     * @param normal The normal temperature range.
     * @param record The record temperature range.
     * @param precipitation The precipitation of the weather record.
     */
    public WeatherRecord(Date d, TemperatureRange today, TemperatureRange normal,
                         TemperatureRange record, double precipitation) {
        this.d = d;
        this.today = today;
        this.normal = normal;
        this.record = record;
        this.precipitation = precipitation;
    }

    /**
     * Checks if today's temperature range is within the normal temperature range.
     * @return Returns true if today's temperature range is within the normal temperature range, false otherwise.
     */
    public boolean withinRange() {
        return this.today.within(this.normal);
    }

    /**
     * Checks if it is a rainy day based on a given precipitation threshold.
     * @param thatPrecipitation The precipitation threshold to determine if it is a rainy day.
     * @return Returns true if it is a rainy day (precipitation >= threshold), false otherwise.
     */
    public boolean rainyDay(double thatPrecipitation) {
        return this.precipitation >= thatPrecipitation;
    }

    /**
     * Checks if today's temperature range is outside of the record temperature range.
     * @return Returns true if today's temperature range is outside of the record temperature range, false otherwise.
     */
    public boolean recordDay() {
        return !this.today.within(this.record);
    }
}