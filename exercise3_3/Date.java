package exercise3_3;

public class Date {
    private int day;
    private int month;
    private int year;

    /**
     * Constructor for the Date class. Initializes the day, month, and year of the date.
     * @param day The day of the date.
     * @param month The month of the date.
     * @param year The year of the date.
     */
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}