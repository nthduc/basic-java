package exercise1_4;

import junit.framework.TestCase;

/**
 * Test case for the Time class using JUnit 4
 */
public class TimeTest extends TestCase {
    /**
     * this is testing for Constructor
     */
    public void testConstructorTime() {
        new Time(0, 0, 0);
        new Time(12, 30, 45);
        new Time(23, 59, 59);
    }
}