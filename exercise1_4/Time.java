package exercise1_4;

/**
 * Time class to represent points in time since midnight
 */
public class Time {
    private int hours;
    private int minutes;
    private int seconds;

    /**
     * Constructor for the Time class
     * @param hours Hours since midnight
     * @param minutes Minutes since midnight
     * @param seconds Seconds since midnight
     */
    public Time(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
}