package exercise1_5;

import junit.framework.TestCase;

/**
 * Test case for the House class using JUnit 4
 */
public class HouseTest extends TestCase {
    /**
     * this is testing for Constructor and getter methods for House class and Address class.
     */
    public void testConstructorAndGetters() {
        // Create an instance of an Address using its constructor.
        Address addr1 = new Address(23, "Maple Street", "Brookline");
        
        // Test that getter methods for Address class return correct values.
        assertEquals(23, addr1.getHouseNumber());
        assertEquals("Maple Street", addr1.getStreetName());
        assertEquals("Brookline", addr1.getCity());

        // Create an instance of a House using its constructor.
        House h1 = new House("Ranch", 7, 375000, addr1);

        // Test that getter methods for House class return correct values.
        assertEquals("Ranch", h1.getKind());
        assertEquals(7, h1.getNumberOfRooms());
        assertEquals(375000, h1.getAskingPrice());
        
        // Test that getAddress method returns correct object.
        assertSame(addr1, h1.getAddress());
        
        // Test that getter methods for returned Address object return correct values.
        assertEquals(23, h1.getAddress().getHouseNumber());
        assertEquals("Maple Street", h1.getAddress().getStreetName());
        assertEquals("Brookline", h1.getAddress().getCity());
        
        // Create another instance of an Address using its constructor.
        Address addr2 = new Address(5, "Joye Road", "Newton");
        
         // Test that getter methods for Address class return correct values.
         assertEquals(5, addr2.getHouseNumber());
         assertEquals("Joye Road", addr2.getStreetName());
         assertEquals("Newton", addr2.getCity());

         // Create another instance of a House using its constructor.
         House h2 = new House("Colonial", 9, 450000, addr2);

         // Test that getter methods for House class return correct values.
         assertEquals("Colonial", h2.getKind());
         assertEquals(9, h2.getNumberOfRooms());
         assertEquals(450000, h2.getAskingPrice());
         
         // Test that getAddress method returns correct object.
         assertSame(addr2, h2.getAddress());
         
         // Test that getter methods for returned Address object return correct values.
         assertEquals(5, h2.getAddress().getHouseNumber());
         assertEquals("Joye Road", h2.getAddress().getStreetName());
         assertEquals("Newton", h2.getAddress().getCity());
    }
}