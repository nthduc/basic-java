package exercise1_5;

/**
 * Address class to represent an address
 */
public class Address {
	private int houseNumber;
	private String streetName;
	private String city;

	/**
	 * Constructor for the Address class
	 * 
	 * @param houseNumber House number of the address
	 * @param streetName  Street name of the address
	 * @param city        City of the address
	 */
	public Address(int houseNumber, String streetName, String city) {
		this.houseNumber = houseNumber;
		this.streetName = streetName;
		this.city = city;
	}

	/**
	 * Returns the house number of the address
	 * 
	 * @return House number of the address
	 */
	public int getHouseNumber() {
		return houseNumber;
	}

	/**
	 * Returns the street name of the address
	 * 
	 * @return Street name of the address
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Returns the city of the address
	 * 
	 * @return City of the address
	 */
	public String getCity() {
		return city;
	}
}