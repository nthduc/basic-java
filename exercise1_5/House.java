package exercise1_5;

/**
 * House class to represent a house
 */
public class House {
    private String kind;
    private int numberOfRooms;
    private int askingPrice; // in dollars
    private Address address;

    /**
     * Constructor for the House class
     * @param kind Kind of the house
     * @param numberOfRooms Number of rooms in the house
     * @param askingPrice Asking price of the house in dollars
     * @param address Address of the house
     */
    public House(String kind, int numberOfRooms, int askingPrice, Address address) {
        this.kind = kind;
        this.numberOfRooms = numberOfRooms;
        this.askingPrice = askingPrice;
        this.address = address;
    }

    /**
     * Returns the kind of the house
     * @return Kind of the house
     */
    public String getKind() {
        return kind;
    }

    /**
     * Returns the number of rooms in the house
     * @return Number of rooms in the house
     */
    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    /**
     * Returns the asking price of the house in dollars
     * @return Asking price of the house in dollars
     */
    public int getAskingPrice() {
        return askingPrice;
    }

    /**
     * Returns the address of the house
     * @return Address of the house
     */
    public Address getAddress() {
        return address;
    }
}
