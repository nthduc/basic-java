package exercise5_9;

public interface Inventory {

	public boolean contains(String toyName);

	public boolean isBelow(double threshold);

	public int howMany();
	
	public Inventory raisePrice(double rate);
}
