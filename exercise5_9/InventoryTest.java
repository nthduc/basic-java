package exercise5_9;

import junit.framework.TestCase;

public class InventoryTest extends TestCase {
	Toy doll = new Toy("doll", 17.95, 5);
	Toy robot = new Toy("robot", 22.05, 3);
	Toy gun = new Toy("gun", 15.0, 4);

	Inventory empty = new MTInventory();
	Inventory r1 = new ConsInventory(gun, empty);
	Inventory r2 = new ConsInventory(robot, r1);
	Inventory r3 = new ConsInventory(doll, r2);

	Inventory all = new ConsInventory(new Toy("gun", 15.0, 4), new ConsInventory(new Toy("robot", 22.05, 3),
			new ConsInventory(new Toy("doll", 17.95, 5), new MTInventory())));

	public void testConstructor() {
		Toy doll = new Toy("doll", 17.95, 5);
		Toy robot = new Toy("robot", 22.05, 3);
		Toy gun = new Toy("gun", 15.0, 4);

		Inventory empty = new MTInventory();
		Inventory r1 = new ConsInventory(gun, empty);
		Inventory r2 = new ConsInventory(robot, r1);
		Inventory r3 = new ConsInventory(doll, r2);

		Inventory all = new ConsInventory(new Toy("gun", 15.0, 4), new ConsInventory(new Toy("robot", 22.05, 3),
				new ConsInventory(new Toy("doll", 17.95, 5), new MTInventory())));
		System.out.println("Danh sách đồ chơi trước khi thay đổi giá: ");
		System.out.println(all);
		System.out.println("Danh sách đồ chơi sau khi thay đổi giá: " + "\n" + r3.raisePrice(0.05));
	}

	public void testContains() {
		assertFalse(empty.contains("doll"));
		assertFalse(r1.contains("robot"));
		assertTrue(r2.contains("gun"));
		assertTrue(r3.contains("doll"));
	}

	public void testIsBelow() {
		assertTrue(empty.isBelow(20.0));
		assertTrue(r1.isBelow(20.0));
		assertFalse(r2.isBelow(20.0));
		assertFalse(r3.isBelow(20.0));
	}

	public void testHowMany() {
		assertEquals(empty.howMany(), 0);
		assertEquals(r1.howMany(), 1);
		assertEquals(r2.howMany(), 2);
		assertEquals(r3.howMany(), 3);
	}

	public void testRaisePrice() {
		assertEquals(empty.raisePrice(0.05), new MTInventory());
		assertEquals(r1.raisePrice(0.05), new ConsInventory(new Toy("gun", 15.75, 4), empty));
		assertEquals(r2.raisePrice(0.05), new ConsInventory(new Toy("robot", 23.1525, 3),
				new ConsInventory(new Toy("gun", 15.75, 4), new MTInventory())));
		assertEquals(r3.raisePrice(0.05),
				new ConsInventory(new Toy("doll", 18.8475, 5), new ConsInventory(new Toy("robot", 23.1525, 3),
						new ConsInventory(new Toy("gun", 15.75, 4), new MTInventory()))));
		
	}

}
