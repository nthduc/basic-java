package exercise5_9;

public class Toy {
	private String name;
	private double price;
	private int available;

	public Toy(String name, double price, int available) {
		this.name = name;
		this.price = price;
		this.available = available;

	}

	public boolean getName(String toyName) {
		return this.name.equals(toyName);
	}

	public boolean isPrice(double threshold) {
		return this.price < threshold;
	}

	public Toy raiseRate(double rate) {
		return new Toy(this.name, this.price + this.price*rate, this.available);
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Toy))
			return false;
		else {
			Toy that = (Toy) obj;
			return this.name == that.name && this.price == that.price && this.available == that.available;
		}

	}

	@Override
	public String toString() {
		return "Toy [name: " + name + ", price: " + price + ", available: " + available + "]";
	}

}
