package exercise5_9;

public class ConsInventory implements Inventory {
	private Toy first;
	private Inventory rest;

	public ConsInventory(Toy first, Inventory rest) {
		super();
		this.first = first;
		this.rest = rest;
	}

	@Override
	public boolean contains(String toyName) {
		return this.first.getName(toyName) || this.rest.contains(toyName);
	}

	@Override
	public boolean isBelow(double threshold) {
		return this.first.isPrice(threshold) && this.rest.isBelow(threshold);
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ConsInventory))
			return false;
		else {
			ConsInventory that = (ConsInventory) obj;
			return this.first.equals(that.first) && this.rest.equals(that.rest);
		}
	}

	public String toString() {
		return this.first.toString() + "\n" + this.rest.toString();
	}

	@Override
	public int howMany() {
		return 1 + this.rest.howMany();
	}

	@Override
	public Inventory raisePrice(double rate) {
		return new ConsInventory(this.first.raiseRate(rate), this.rest.raisePrice(rate)) ;
	}

}
