package exercise5_9;

public class MTInventory implements Inventory {
	public String toString() {
		return "";
	}

	@Override
	public boolean contains(String toyName) {
		return false;
	}

	@Override
	public boolean isBelow(double threshold) {
		return true;
	}

	public int howMany() {
		return 0;
	}

	public Inventory raisePrice(double rate) {
		return new MTInventory();
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof MTInventory))
			return false;
		return true;
	}
}
