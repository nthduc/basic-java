package exercise4_6;

import junit.framework.TestCase;

public class ItemTest extends TestCase {
	AnItem ice = new IceCream("Oc que sau rieng", 100.0, 1200.0, "sau rieng", "package 1");
	AnItem coffee = new Coffee("Nest Coffee", 10.0, 100.0, "kind 1");
	AnItem juice = new Juice("Orange", 50.0, 70000.0, "orange", "package 2");

	public void testUnitPrice() {
		assertEquals(ice.unitPrice(), 12.0);
		assertEquals(coffee.unitPrice(), 10.0);
		assertEquals(juice.unitPrice(), 1400.0, 0.01);
	}

	public void testLowerPrice() {
		assertTrue(ice.lowPrice(2000.0));
		assertTrue(coffee.lowPrice(200.0));
		assertFalse(juice.lowPrice(1000.0));
	}

	public void testCheaperThan() {
		assertTrue(ice.cheaperThan(juice));
		assertTrue(coffee.cheaperThan(juice));
		assertFalse(juice.cheaperThan(coffee));
	}
}
