package exercise4_6;

public class Juice extends AnItem {
	private String flavor;
	private String pack;

	public Juice(String name, double weight, double price, String flavor, String pack) {
		super(name, weight, price);
		this.flavor = flavor;
		this.pack = pack;
	}
}