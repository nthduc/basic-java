package exercise4_6;

public abstract class AnItem {
	protected String name;
	protected double weight;
	protected double price;

	public AnItem(String name, double weight, double price) {
		this.name = name;
		this.weight = weight;
		this.price = price;
	}

	// tinh don gia
	public double unitPrice() {
		return this.price / this.weight;
	}

	// kiem tra gia cua mot so mat hang co thap hon so luong truyen vao khong
	public boolean lowPrice(double amount) {
		return this.unitPrice() < amount;
	}

	// kiem tra gia cua 1 mat hang co re hon mot vai mat hang khac khong
	public boolean cheaperThan(AnItem other) {
		return this.price < other.price;
	}
}
