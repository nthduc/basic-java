package exercise4_6;

public class Coffee extends AnItem {
	private String lable;

	public Coffee(String name, double weight, double price, String lable) {
		super(name, weight, price);
		this.lable = lable;
	}

}
