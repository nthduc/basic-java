package exercise4_6;

public class IceCream extends AnItem {
	private String flavor;
	private String pack;

	public IceCream(String name, double weight, double price, String flavor, String pack) {
		super(name, weight, price);
		this.flavor = flavor;
		this.pack = pack;
	}

}
