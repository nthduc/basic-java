package baigiang5_1_restaurant;

/**
 * Class representing a restaurant with a name, food type, price range, and intersection.
 */
public class Restaurant {
    private String name;
    private String food;
    private String priceRange;
    private Intersection intersection;

    /**
     * Constructor for the Restaurant class. Initializes the name, food type, price range, and intersection of the restaurant.
     * @param name The name of the restaurant.
     * @param food The food type of the restaurant.
     * @param priceRange The price range of the restaurant.
     * @param intersection The intersection where the restaurant is located.
     */
    public Restaurant(String name, String food,
                      String priceRange,
                      Intersection intersection) {
        this.name = name;
        this.food = food;
        this.priceRange = priceRange;
        this.intersection = intersection;
    }

    /**
     * Returns a string representation of the restaurant.
     * @return A string representation of the restaurant with its fields separated by commas.
     */
    public String toString() {
        return "Name: " + this.name + ", food: " + this.food
                + ", range price: " + this.priceRange
                + ", intersection: " + this.intersection.toString() + "\n";
    }

    /**
     * Checks if this restaurant is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this restaurant is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Restaurant))
            return false;
        else {
            Restaurant that = (Restaurant) obj;
            return this.name.equals(that.name) &&
                    this.food.equals(that.food) &&
                    this.priceRange.equals(that.priceRange) &&
                    this.intersection.equals(that.intersection);
        }
    }
}