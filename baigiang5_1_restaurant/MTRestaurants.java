package baigiang5_1_restaurant;

/**
 * Class representing an empty list of restaurants.
 */
public class MTRestaurants implements IRestaurants {

    /**
     * Returns a string representation of an empty list of restaurants (an empty string).
     * @return An empty string representing an empty list of restaurants.
     */
    public String toString() {
        return "";
    }

    /**
     * Checks if this empty list of restaurants is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this empty list is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MTRestaurants))
            return false;
        return true;
    }
}