package baigiang5_1_restaurant;

import junit.framework.TestCase;

/**
 * Class for testing the IRestaurants classes.
 */
public class LoRestaurantsTest extends TestCase {

    /**
     * Tests the constructor of the ConsRestaurants class.
     */
    public void testConstructor() {
        // Create some restaurants
        Restaurant r1 = new Restaurant("Chez Nous",
                "French", "exp.", new Intersection(7, 65));
        Restaurant r2 = new Restaurant("Das Bier",
                "German", "cheap", new Intersection(2, 86));
        Restaurant r3 = new Restaurant("Sun",
                "Chinese", "cheap", new Intersection(10, 13));

        // Create an empty list of restaurants
        IRestaurants empty = new MTRestaurants();

        // Create a list with one restaurant
        IRestaurants l1 = new ConsRestaurants(r3, empty);

        // Create a list with two restaurants
        IRestaurants l2 = new ConsRestaurants(r2, l1);

        // Create a list with three restaurants
        IRestaurants l3 = new ConsRestaurants(r1, l2);

        // Print the list with three restaurants
        System.out.println(l3);

        // Create a list with all three restaurants using a different method
        IRestaurants all = new ConsRestaurants(r1,
                new ConsRestaurants(r2,
                        new ConsRestaurants(r3, new MTRestaurants())));

        // Check if the two lists are equal
        assertEquals(all, l3);
    }
}
