package baigiang5_1_restaurant;

/**
 * Class representing an intersection with an avenue and a street.
 */
public class Intersection {
    private int avenue;
    private int street;

    /**
     * Constructor for the Intersection class. Initializes the avenue and street of the intersection.
     * @param avenue The avenue of the intersection.
     * @param street The street of the intersection.
     */
    public Intersection(int avenue, int street) {
        this.avenue = avenue;
        this.street = street;
    }

    /**
     * Returns a string representation of the intersection.
     * @return A string representation of the intersection with its fields separated by commas.
     */
    public String toString() {
        return "avenue: " + this.avenue
                + ", street: " + this.street;
    }

    /**
     * Checks if this intersection is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this intersection is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Intersection))
            return false;
        else {
            Intersection that = (Intersection) obj;
            return this.avenue == that.avenue &&
                    this.street == that.street;
        }
    }
}