package baigiang5_1_restaurant;

/**
 * Class representing a non-empty list of restaurants.
 */
public class ConsRestaurants implements IRestaurants {
    private Restaurant first;
    private IRestaurants rest;

    /**
     * Constructor for the ConsRestaurants class. Initializes the first restaurant and the rest of the list.
     * @param first The first restaurant in the list.
     * @param rest The rest of the list.
     */
    public ConsRestaurants(Restaurant first, IRestaurants rest) {
        this.first = first;
        this.rest = rest;
    }

    /**
     * Returns a string representation of the list of restaurants.
     * @return A string representation of the list with each restaurant on a separate line.
     */
    public String toString() {
        return this.first.toString() + " \n" + this.rest.toString();
    }

    /**
     * Checks if this list of restaurants is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this list is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ConsRestaurants))
            return false;
        else {
            ConsRestaurants that = (ConsRestaurants) obj;
            return this.first.equals(that.first)
                    && this.rest.equals(that.rest);
        }
    }
}
