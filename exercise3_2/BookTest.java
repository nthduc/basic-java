package exercise3_2;
import org.junit.Test;

import junit.framework.TestCase;


/**
 * Test case for the Book class using JUnit 4
 */
public class BookTest extends TestCase {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorBook() {
        new Book("The Great Gatsby", 10.0, 1925, new Author("F. Scott Fitzgerald", 1896));
        new Book("To Kill a Mockingbird", 12.0, 1960, new Author("Harper Lee", 1926));
        new Book("Pride and Prejudice", 15.0, 1813, new Author("Jane Austen", 1775));
    }

    /**
     * this is testing for currentBook function
     */
    @Test
    public void testCurrentBook() {
        assertFalse(new Book("The Great Gatsby", 10.0, 1925, new Author("F. Scott Fitzgerald", 1896)).currentBook());
        assertFalse(new Book("To Kill a Mockingbird", 12.0, 1960, new Author("Harper Lee", 1926)).currentBook());
        assertFalse(new Book("Pride and Prejudice", 15.0, 1813, new Author("Jane Austen", 1775)).currentBook());
    }

    /**
     * this is testing for currentAuthor function
     */
    @Test
    public void testCurrentAuthor() {
        assertFalse(new Book("The Great Gatsby", 10.0, 1925, new Author("F. Scott Fitzgerald", 1896)).currentAuthor());
        assertTrue(new Book("To Kill a Mockingbird", 12.0, 1960, new Author("Harper Lee", 1926)).currentAuthor());
        assertFalse(new Book("Pride and Prejudice", 15.0, 1813, new Author("Jane Austen", 1775)).currentAuthor());
    }

    /**
     * this is testing for thisAuthor function
     */
    @Test
    public void testThisAuthor() {
        assertTrue(new Book("The Great Gatsby", 10.0, 1925, new Author("F. Scott Fitzgerald", 1896)).thisAuthor(new Author("F. Scott Fitzgerald", 1896)));
        assertFalse(new Book("To Kill a Mockingbird", 12.0, 1960, new Author("Harper Lee", 1926)).thisAuthor(new Author("F. Scott Fitzgerald", 1896)));
        assertFalse(new Book("Pride and Prejudice", 15.0, 1813, new Author("Jane Austen", 1775)).thisAuthor(new Author("F. Scott Fitzgerald", 1896)));
    }
    
    @Test
    public void testSameAuthor() {
        assertTrue(new Book("The Great Gatsby", 10.0, 1925, new Author("F. Scott Fitzgerald", 1896)).sameAuthor(new Book("Tender Is the Night", 12.0, 1934, new Author("F. Scott Fitzgerald", 1896))));
        assertFalse(new Book("To Kill a Mockingbird", 12.0, 1960, new Author("Harper Lee", 1926)).sameAuthor(new Book("Tender Is the Night", 12.0, 1934, new Author("F. Scott Fitzgerald", 1896))));
        assertFalse(new Book("Pride and Prejudice", 15.0, 1813, new Author("Jane Austen", 1775)).sameAuthor(new Book("Tender Is the Night", 12.0, 1934, new Author("F. Scott Fitzgerald", 1896))));
    }
    @Test
    public void testSameGeneration() {
        assertTrue(new Book("The Great Gatsby", 10.0, 1925, new Author("F. Scott Fitzgerald", 1896)).sameGeneration(new Book("The Sun Also Rises", 12.0, 1926, new Author("Ernest Hemingway", 1899))));
        assertFalse(new Book("To Kill a Mockingbird", 12.0, 1960, new Author("Harper Lee", 1926)).sameGeneration(new Book("The Sun Also Rises", 12.0, 1926, new Author("Ernest Hemingway", 1899))));
}
}
