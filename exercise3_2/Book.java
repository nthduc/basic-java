package exercise3_2;

public class Book {
    String title;
    double price;
    int yearOfPublication;
    Author author;

    /**
     * Constructor for the Book class
     * @param title Title of the book as a string
     * @param price Price of the book in dollars
     * @param yearOfPublication Year of publication as an integer
     * @param author Author of the book as an Author object
     */
    public Book(String title, double price, int yearOfPublication, Author author) {
        this.title = title;
        this.price = price;
        this.yearOfPublication = yearOfPublication;
        this.author = author;
    }

    /**
     * Checks whether the book was published in 2004 or 2003
     * @return true if the book was published in 2004 or 2003, false otherwise
     */
    public boolean currentBook() {
        return this.yearOfPublication == 2004 || this.yearOfPublication == 2003;
    }

    /**
     * Determines whether the book was written by a current author (born after 1940)
     * @return true if the book was written by a current author, false otherwise
     */
    public boolean currentAuthor() {
        return this.author.birthYear > 1940;
    }

    /**
     * Determines whether the book was written by the specified author
     * @param otherAuthor the specified author to compare with
     * @return true if the book was written by the specified author, false otherwise
     */
    public boolean thisAuthor(Author otherAuthor) {
        return this.author.equals(otherAuthor);
    }

    /**
     * Determines whether one book was written by the same author as some other book
     * @param otherBook the other book to compare with
     * @return true if one book was written by the same author as some other book, false otherwise
     */
    public boolean sameAuthor(Book otherBook) {
        return this.author.equals(otherBook.author);
    }

    /**
     * Determines whether two books were written by two authors born less than 10 years apart
     * @param otherBook the other book to compare with
     * @return true if two books were written by two authors born less than 10 years apart, false otherwise
     */
    public boolean sameGeneration(Book otherBook) {
        return Math.abs(this.author.birthYear - otherBook.author.birthYear) < 10;
    }
}