package exercise3_2;

public class Author {
    String name;
    int birthYear;

    /**
     * Constructor for the Author class
     * @param name Name of the author as a string
     * @param birthYear Birth year of the author as an integer
     */
    public Author(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    /**
     * Determines whether this author is equal to some other author
     * @param obj the other object to compare with
     * @return true if this author is equal to some other author, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Author) {
            Author otherAuthor = (Author) obj;
            return this.name.equals(otherAuthor.name) && this.birthYear == otherAuthor.birthYear;
        } else {
            return false;
        }
    }
}