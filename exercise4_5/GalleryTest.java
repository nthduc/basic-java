package exercise4_5;

import junit.framework.TestCase;

public class GalleryTest extends TestCase {
	AGallery image = new Image("image.png", 57234, 100, 50, "medium");
	AGallery text = new Text("text.txt", 5312, 830);
	AGallery sound = new Sound("song.mp3", 40960, new Time(3, 20));

	public void testTimeToDownload() {
		assertEquals(image.timeToDownload(100), 572.0);
		assertEquals(text.timeToDownload(1000), 5.0);
		assertEquals(sound.timeToDownload(10), 4096.0);
	}

	public void testSmallerThan() {
		assertTrue(image.smallerThan(60000));
		assertTrue(text.smallerThan(1000000));
		assertFalse(sound.smallerThan(100));
	}

	public void testSameName() {
		assertTrue(image.sameName("image.png"));
		assertTrue(text.sameName("text.txt"));
		assertFalse(sound.sameName("song.mp4"));
	}
}
