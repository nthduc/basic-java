package exercise4_5;

public abstract class AGallery {
	protected String name;
	protected int size;

	public AGallery(String name, int size) {
		this.name = name;
		this.size = size;
	}

	// tinh thoi gian dowload
	public double timeToDownload(int speed) {
		return this.size / speed;
	}

	// kiem tra kich thuoc nho hon hay khong
	public boolean smallerThan(int size) {
		return this.size < size;
	}

	// kiem tra ten cos giong nhau khong
	public boolean sameName(String name) {
		return this.name.equals(name);
	}
}
