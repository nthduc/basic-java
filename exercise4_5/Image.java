package exercise4_5;

public class Image extends AGallery {
	private int height;
	private int width;
	private String quality;

	public Image(String sourece, int size, int height, int width, String quality) {
		super(sourece, size);
		this.height = height;
		this.width = width;
		this.quality = quality;
	}

}
