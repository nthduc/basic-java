package exercise2_6;

/**
 * Quadratic class to represent a quadratic equation
 */
public class Quadratic {
    double a;
    double b;
    double c;

    /**
     * Constructor for the Quadratic class
     * @param a Coefficient of the x^2 term
     * @param b Coefficient of the x term
     * @param c Constant term
     */
    public Quadratic(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Computes the discriminant of the quadratic equation
     * @return the value of the discriminant
     */
    double computeDelta() {
        return this.b * this.b - 4 * this.a * this.c;
    }

    /**
     * Determines the kind of solutions the quadratic equation has
     * @return "degenerate" if a is 0, "none" if the discriminant is negative, "one solution" if the discriminant is 0, and "two solutions" if the discriminant is positive
     */
    String whatKind() {
        double delta = this.computeDelta();
        if (this.a == 0) return "degenerate";
        if (delta < 0) return "none";
        if (delta == 0) return "one solution";
        return "two solutions";
    }
}
