package exercise2_6;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the Quadratic class using JUnit 4
 */
public class QuadraticTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorQuadratic() {
        new Quadratic(1.0, 2.0, 1.0);
        new Quadratic(1.0, -3.0, 2.0);
        new Quadratic(1.0, -7.0, 10.0);
    }

    /**
     * this is testing for computeDelta function
     */
    @Test
    public void testComputeDelta() {
        assertEquals(0.0, new Quadratic(1.0, 2.0, 1.0).computeDelta(), 0.01);
        assertEquals(1.0, new Quadratic(1.0, -3.0, 2.0).computeDelta(), 0.01);
        assertEquals(-3.0, new Quadratic(1.0, -7.0, 10.0).computeDelta(), 0.01);
    }

    /**
     * this is testing for whatKind function
     */
    @Test
    public void testWhatKind() {
        assertEquals("one solution", new Quadratic(1.0, 2.0, 1.0).whatKind());
        assertEquals("two solutions", new Quadratic(1.0, -3.0, 2.0).whatKind());
        assertEquals("none", new Quadratic(1.0, -7.0, 10.0).whatKind());
    }
}
