package exercise1_8;

/**
 * Train class to represent a train
 */
public class Train {
	private Schedule schedule;
	private Route route;
	private boolean local;

	/**
	 * Constructor for the Train class
	 * 
	 * @param schedule Schedule of the train
	 * @param route    Route of the train
	 * @param local    Whether the train is local or not
	 */
	public Train(Schedule schedule, Route route, boolean local) {
		this.schedule = schedule;
		this.route = route;
		this.local = local;
	}

	/**
	 * Returns the schedule of the train
	 * 
	 * @return Schedule of the train
	 */
	public Schedule getSchedule() {
		return schedule;
	}

	/**
	 * Returns the route of the train
	 * 
	 * @return Route of the train
	 */
	public Route getRoute() {
		return route;
	}

	/**
	 * Returns whether the train is local or not
	 * 
	 * @return Whether the train is local or not
	 */
	public boolean isLocal() {
		return local;
	}
}