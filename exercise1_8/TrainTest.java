package exercise1_8;

import junit.framework.TestCase;

/**
 * Test case for Train class using JUnit 4.
 */
public class TrainTest extends TestCase {

	/**
	 * This is testing for Constructor and getter methods for Train class and its
	 * inner classes.
	 */
	public void testConstructorAndGetters() {

		// Create an instance of a ClockTime using its constructor.
		ClockTime ct1 = new ClockTime(10, 30);

		// Test that getter methods for ClockTime class return correct values.
		assertEquals(10, ct1.getHour());
		assertEquals(30, ct1.getMinute());

		// Create another instance of a ClockTime using its constructor.
		ClockTime ct2 = new ClockTime(12, 45);

		// Test that getter methods for ClockTime class return correct values.
		assertEquals(12, ct2.getHour());
		assertEquals(45, ct2.getMinute());

		// Create an instance of a Schedule using its constructor.
		Schedule s1 = new Schedule(ct1, ct2);

		// Test that getter methods for Schedule class return correct values.
		assertSame(ct1, s1.getDeparture());
		assertSame(ct2, s1.getArrival());

		// Test that getter methods for returned ClockTime objects return correct
		// values.
		assertEquals(10, s1.getDeparture().getHour());
		assertEquals(30, s1.getDeparture().getMinute());
		assertEquals(12, s1.getArrival().getHour());
		assertEquals(45, s1.getArrival().getMinute());

		// Create an instance of a Route using its constructor.
		Route r1 = new Route("New York", "Boston");

		// Test that getter methods for Route class return correct values.
		assertEquals("New York", r1.getOrigin());
		assertEquals("Boston", r1.getDestination());

		// Create an instance of a Train using its constructor.
		Train t1 = new Train(s1, r1, true);

		// Test that getter methods for Train class return correct values.
		assertSame(s1, t1.getSchedule());
		assertSame(r1, t1.getRoute());
		assertTrue(t1.isLocal());

		// Test that getter methods for returned Schedule object return correct values.
		assertSame(ct1, t1.getSchedule().getDeparture());
		assertSame(ct2, t1.getSchedule().getArrival());

		// Test that getter methods for returned ClockTime objects return correct
		// values.
		assertEquals(10, t1.getSchedule().getDeparture().getHour());
		assertEquals(30, t1.getSchedule().getDeparture().getMinute());
		assertEquals(12, t1.getSchedule().getArrival().getHour());
		assertEquals(45, t1.getSchedule().getArrival().getMinute());

		// Test that getter methods for returned Route object return correct values.
		assertEquals("New York", t1.getRoute().getOrigin());
		assertEquals("Boston", t1.getRoute().getDestination());

		// Create another instance of a ClockTime using its constructor.
		ClockTime ct3 = new ClockTime(14, 0);

		// Test that getter methods for ClockTime class return correct values.
		assertEquals(14, ct3.getHour());
		assertEquals(0, ct3.getMinute());

		// Create another instance of a ClockTime using its constructor.
		ClockTime ct4 = new ClockTime(16, 15);

		// Test that getter methods for ClockTime class return correct values.
		assertEquals(16, ct4.getHour());
		assertEquals(15, ct4.getMinute());

		// Create another instance of a Schedule using its constructor.
		Schedule s2 = new Schedule(ct3, ct4);

		// Test that getter methods for Schedule class return correct values.
		assertSame(ct3, s2.getDeparture());
		assertSame(ct4, s2.getArrival());

		// Test that getter methods for returned ClockTime objects return correct
		// values.
		assertEquals(14, s2.getDeparture().getHour());
		assertEquals(0, s2.getDeparture().getMinute());
		assertEquals(16, s2.getArrival().getHour());
		assertEquals(15, s2.getArrival().getMinute());

		// Create another instance of a Route using its constructor.
		Route r2 = new Route("Boston", "Washington D.C.");

		// Test that getter methods for Route class return correct values.
		assertEquals("Boston", r2.getOrigin());
		assertEquals("Washington D.C.", r2.getDestination());

		// Create another instance of a Train using its constructor.
		Train t2 = new Train(s2, r2, false);

		// Test that getter methods for Train class return correct values.
		assertSame(s2, t2.getSchedule());
		assertSame(r2, t2.getRoute());
		assertFalse(t2.isLocal());

		// Test that getter methods for returned Schedule object return correct values.
		assertSame(ct3, t2.getSchedule().getDeparture());
		assertSame(ct4, t2.getSchedule().getArrival());

		// Test that getter methods for returned ClockTime objects return correct
		// values.
		assertEquals(14, t2.getSchedule().getDeparture().getHour());
		assertEquals(0, t2.getSchedule().getDeparture().getMinute());
		assertEquals(16, t2.getSchedule().getArrival().getHour());
		assertEquals(15, t2.getSchedule().getArrival().getMinute());

		// Test that getter methods for returned Route object return correct values.
		assertEquals("Boston", t2.getRoute().getOrigin());
		assertEquals("Washington D.C.", t2.getRoute().getDestination());
	}
}