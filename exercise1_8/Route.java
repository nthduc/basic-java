package exercise1_8;

/**
 * Route class to represent a route
 */
class Route {
    private String origin;
    private String destination;

    /**
     * Constructor for the Route class
     * @param origin Origin station of the route
     * @param destination Destination station of the route
     */
    public Route(String origin, String destination) {
        this.origin = origin;
        this.destination = destination;
    }

    /**
     * Returns the origin station of the route
     * @return Origin station of the route
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Returns the destination station of the route
     * @return Destination station of the route
     */
    public String getDestination() {
        return destination;
    }
}