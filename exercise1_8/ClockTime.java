package exercise1_8;

/**
 * ClockTime class to represent a clock time
 */
public class ClockTime {
    private int hour;
    private int minute;

    /**
     * Constructor for the ClockTime class
     * @param hour Hour of the clock time
     * @param minute Minute of the clock time
     */
    public ClockTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    /**
     * Returns the hour of the clock time
     * @return Hour of the clock time
     */
    public int getHour() {
        return hour;
    }

    /**
     * Returns the minute of the clock time
     * @return Minute of the clock time
     */
    public int getMinute() {
        return minute;
    }
}
