package exercise1_8;

/**
 * Schedule class to represent a schedule
 */
public class Schedule {
    private ClockTime departure;
    private ClockTime arrival;

    /**
     * Constructor for the Schedule class
     * @param departure Departure time of the schedule
     * @param arrival Arrival time of the schedule
     */
    public Schedule(ClockTime departure, ClockTime arrival) {
        this.departure = departure;
        this.arrival = arrival;
    }

    /**
     * Returns the departure time of the schedule
     * @return Departure time of the schedule
     */
    public ClockTime getDeparture() {
        return departure;
    }

    /**
     * Returns the arrival time of the schedule
     * @return Arrival time of the schedule
     */
    public ClockTime getArrival() {
        return arrival;
    }
}