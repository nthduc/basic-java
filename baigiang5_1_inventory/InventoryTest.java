package baigiang5_1_inventory;

import junit.framework.TestCase;

/**
 * Class for testing the Inventory classes.
 */
public class InventoryTest extends TestCase {

    /**
     * Tests the constructor of the ConsInventory class.
     */
    public void testConstructor() {
        // Create some toys
        Toy doll = new Toy("doll", 17.95, 5);
        Toy robot = new Toy("robot", 22.05, 3);
        Toy gun = new Toy("gun", 15.0, 4);

        // Create an empty inventory
        Inventory empty = new MTInventory();

        // Create an inventory with one toy
        Inventory i1 = new ConsInventory(gun, empty);

        // Create an inventory with two toys
        Inventory i2 = new ConsInventory(robot, i1);

        // Create an inventory with three toys
        Inventory i3 = new ConsInventory(doll, i2);

        // Print the inventory with three toys
        System.out.println(i3);

        // Create an inventory with all three toys using a different method
        Inventory all = new ConsInventory(doll,
                new ConsInventory(robot,
                        new ConsInventory(gun, new MTInventory())));

        // Print the inventory with all three toys
        System.out.println(all);
    }
}
