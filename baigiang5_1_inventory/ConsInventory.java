package baigiang5_1_inventory;

/**
 * Class representing a non-empty inventory.
 */
public class ConsInventory implements Inventory {
    private Toy first;
    private Inventory rest;

    /**
     * Constructor for the ConsInventory class. Initializes the first toy and the rest of the inventory.
     * @param first The first toy in the inventory.
     * @param rest The rest of the inventory.
     */
    public ConsInventory(Toy first, Inventory rest) {
        this.first = first;
        this.rest = rest;
    }

    /**
     * Returns a string representation of the inventory.
     * @return A string representation of the inventory with each toy on a separate line.
     */
    public String toString() {
        return this.first.toString() + "\n"
                + this.rest.toString();
    }
}