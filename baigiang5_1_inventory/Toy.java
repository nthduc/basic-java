package baigiang5_1_inventory;

/**
 * Class representing a toy in an inventory.
 */
public class Toy {
    private String name;
    private double price;
    private int available;

    /**
     * Constructor for the Toy class. Initializes the name, price, and availability of the toy.
     * @param name The name of the toy.
     * @param price The price of the toy.
     * @param available The number of available units of the toy.
     */
    public Toy(String name, double price,
               int available) {
        this.name = name;
        this.price = price;
        this.available = available;
    }

    /**
     * Returns a string representation of the toy.
     * @return A string representation of the toy with its fields separated by commas.
     */
    public String toString() {
        return "name: " + this.name
                + ", price: " + this.price
                + ", available: " + this.available;
    }
}