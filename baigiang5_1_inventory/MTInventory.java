package baigiang5_1_inventory;

/**
 * Class representing an empty inventory.
 */
public class MTInventory implements Inventory {

    /**
     * Returns a string representation of an empty inventory (an empty string).
     * @return An empty string representing an empty inventory.
     */
    public String toString() {
        return "";
    }
}