package exercise5_10;

public class From {
	private Date date;

	public From(Date date) {
		this.date = date;
	}

	// kiem tra ngay hom nay
	public boolean byShorterDays(From first) {
		if (this.date.checkDate(first.date))
			return true;
		return false;
	}

	// kiem tra ngay tu lon den nho
	public boolean byShorterDaysLargeToSmall(From first) {
		if (this.date.checkDateLargeToSmall(first.date))
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "From [date=" + date + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		From other = (From) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}
}
