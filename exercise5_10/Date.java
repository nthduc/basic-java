package exercise5_10;

public class Date {
	private int day;
	private int month;
	private int year;

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	// kiem tra ngay
	public boolean checkDate(Date date) {
		if (this.day < date.day)
			return true;
		return false;
	}

	// kiem tra ngay hom nay co lon hon
	public boolean checkDateLargeToSmall(Date date) {
		if (this.day > date.day)
			return true;
		return false;
	}
}
