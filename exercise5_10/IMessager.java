package exercise5_10;

public interface IMessager {

	// sap xep theo ngay nho den lon
	public IMessager sortByDateSmallToLarge();

	// chen theo ngay tu nho den lon
	public IMessager insertInOrderOfDateSmallToLarge(From first);

	// sap xep theo ngay lon den nho
	public IMessager sortByDateLargeToSmall();

	// chen theo ngay tu lon den nho
	public IMessager insertInOrderOfDateLargeToSmall(From first);
}
