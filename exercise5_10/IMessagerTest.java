package exercise5_10;

import junit.framework.TestCase;

public class IMessagerTest extends TestCase {

	// test constructor
	public void testConstructor() {
		Date d1 = new Date(7, 10, 2022);
		Date d2 = new Date(2, 11, 2022);
		Date d3 = new Date(5, 12, 2022);

		IMessager empty = new MTMessager();
		IMessager i1 = new ConsMessager(new From(d1), empty);
		IMessager i2 = new ConsMessager(new From(d2), i1);
		IMessager i3 = new ConsMessager(new From(d3), i2);

		assertEquals(i3.sortByDateSmallToLarge(),
				new ConsMessager(new From(d2), new ConsMessager(new From(d3), new ConsMessager(new From(d1), empty))));
	}

	// test sortByDateLargeSmall
	public void testSortByDateLargeSmall() {
		Date d1 = new Date(7, 10, 2022);
		Date d2 = new Date(2, 11, 2022);
		Date d3 = new Date(5, 12, 2022);

		IMessager empty = new MTMessager();
		IMessager i1 = new ConsMessager(new From(d1), empty);
		IMessager i2 = new ConsMessager(new From(d2), i1);
		IMessager i3 = new ConsMessager(new From(d3), i2);

		System.out.println(i3.sortByDateLargeToSmall());
	}
}
