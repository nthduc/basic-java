package exercise5_10;

import java.util.Objects;

public class ConsMessager implements IMessager {
	private From first;
	private IMessager rest;

	public ConsMessager(From first, IMessager rest) {
		this.first = first;
		this.rest = rest;
	}

	@Override
	public String toString() {
		return "ConsMessager " + first + ", \n" + rest + "";
	}

	@Override
	public int hashCode() {
		return Objects.hash(first, rest);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsMessager other = (ConsMessager) obj;
		return Objects.equals(first, other.first) && Objects.equals(rest, other.rest);
	}

	// sap xep theo ngay tu nho den lon
	@Override
	public IMessager sortByDateSmallToLarge() {
		return this.rest.sortByDateSmallToLarge().insertInOrderOfDateSmallToLarge(this.first);
	}

	// chen theo ngay tu nho den lon
	@Override
	public IMessager insertInOrderOfDateSmallToLarge(From first) {
		if (first.byShorterDays(this.first))
			return new ConsMessager(first, this);
		else
			return new ConsMessager(this.first, this.rest.insertInOrderOfDateSmallToLarge(first));
	}

	// sap xep theo ngay tu lon den nho
	@Override
	public IMessager sortByDateLargeToSmall() {
		return this.rest.sortByDateLargeToSmall().insertInOrderOfDateLargeToSmall(this.first);
	}

	// chen theo ngay tu lon den nho
	@Override
	public IMessager insertInOrderOfDateLargeToSmall(From first) {
		if (first.byShorterDaysLargeToSmall(this.first))
			return new ConsMessager(first, this);
		else
			return new ConsMessager(this.first, this.rest.insertInOrderOfDateLargeToSmall(first));
	}

}
