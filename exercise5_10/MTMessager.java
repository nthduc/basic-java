package exercise5_10;

public class MTMessager implements IMessager {

	@Override
	public String toString() {
		return "";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof MTMessager))
			return false;
		return true;

	}

	@Override
	public IMessager sortByDateSmallToLarge() {
		return new MTMessager();
	}

	@Override
	public IMessager insertInOrderOfDateSmallToLarge(From first) {
		return new ConsMessager(first, new MTMessager());
	}

	@Override
	public IMessager sortByDateLargeToSmall() {
		return new MTMessager();
	}

	@Override
	public IMessager insertInOrderOfDateLargeToSmall(From first) {
		return new ConsMessager(first, new MTMessager());
	}
}
