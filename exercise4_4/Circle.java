package exercise4_4;

public class Circle extends AShape {
	private int radius;

	public Circle(CartPt location, int radius) {
		super(location);
		this.radius = radius;
	}

	@Override
	public double area() {
		return this.radius * this.radius * Math.PI;
	}

	@Override
	public double distanceToO() {
		return Math.abs(this.location.distancetoO() - this.radius);
	}

	@Override
	public boolean contains(CartPt point) {
		return this.location.distanceto(point) <= this.radius;
	}

	@Override
	public Square boundingBox() {
		return new Square(this.location.translate(-this.radius, -this.radius), this.radius * 2);
	}

}
