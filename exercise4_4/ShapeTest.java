package exercise4_4;

import junit.framework.TestCase;

public class ShapeTest extends TestCase {
	IShape dot = new Dot(new CartPt(4, 3));
	IShape square = new Square(new CartPt(4, 3), 30);
	IShape circle = new Circle(new CartPt(5, 5), 20);
	IShape rectangle = new Rectangles(new CartPt(7, 5), 5, 3);
	
	// test area
	public void testArea() {
		assertEquals(dot.area(), 0.0, 0.001);
		assertEquals(square.area(), 900.0, 0.001);
		assertEquals(circle.area(), 1256.637, 0.001);
		assertEquals(rectangle.area(), 15.0, 0.001);
	}
	// test distancetoO
	public void testDistanceToO() {
		assertEquals(dot.distanceToO(), 5.0, 0.001);
		assertEquals(square.distanceToO(), 5.0, 0.001);
		assertEquals(circle.distanceToO(), 12.9289, 0.001);
		assertEquals(rectangle.distanceToO(), 8.6023, 0.001);
	}
	
//	 test contain
	public void testContains() {
		assertTrue(dot.contains(new CartPt(4, 3)));
		assertFalse(square.contains(new CartPt(100, 100)));
		assertTrue(circle.contains(new CartPt(8, 6)));
		assertFalse(rectangle.contains(new CartPt(10, 10)));
	}

//	// test boundingBox
	public void testBoundingBox() {
		assertEquals(dot.boundingBox(), new Square(new CartPt(4, 3), 0));
		assertEquals(square.boundingBox(), new Square(new CartPt(4, 3), 30));
		assertEquals(circle.boundingBox(), new Square(new CartPt(-15, -15), 40));
		assertEquals(rectangle.boundingBox(), new Square(new CartPt(7, 5), 5));
	}
}
