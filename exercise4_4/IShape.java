package exercise4_4;

public interface IShape {
	// tinh dien tich
	public double area();

	// khoang cach den goc toa do O
	public double distanceToO();

	// kiem tra co chua mot vai diem chi dinh hay khong
	public boolean contains(CartPt point);

	// tinh hop gioi han
	public Square boundingBox();

}
