package exercise4_4;

public class Rectangles extends AShape {
	private int height;
	private int width;

	public Rectangles(CartPt location, int height, int width) {
		super(location);
		this.height = height;
		this.width = width;
	}

	@Override
	public double area() {
		return this.height * this.width;
	}

	@Override
	public double distanceToO() {
		return this.location.distancetoO();
	}

	@Override
	public boolean contains(CartPt point) {
		return false;
	}

	@Override
	public Square boundingBox() {
		return new Square(this.location, this.height);
	}

}
