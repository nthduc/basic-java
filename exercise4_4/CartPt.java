package exercise4_4;

public class CartPt {
	private int x;
	private int y;

	public CartPt(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	// tinh khoang cach tu mot diem den goc toa do O
	public double distancetoO() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	// tinh khoang cach tu mot diem den mot diem khac
	public double distanceto(CartPt point) {
		double deltaX = this.x - point.x;
		double deltaY = this.y - point.y;
		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}

	// dich chuyen
	public CartPt translate(int dX, int dY) {
		return new CartPt(this.x + dY, this.y + dY);
	}

	// kiem tra xem mot diem co trung voi mot vai diem chi dinh hay khong
	public boolean equals(Object obj) {
		if (null == obj || !(obj instanceof CartPt))
			return false;
		else {
			CartPt that = (CartPt) obj;
			return (this.x == that.x) && (this.y == that.y);
		}
	}
}
