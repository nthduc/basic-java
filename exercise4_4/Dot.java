package exercise4_4;

public class Dot extends AShape {

	public Dot(CartPt location) {
		super(location);
	}

	@Override
	public double area() {
		return 0;
	}

	@Override
	public boolean contains(CartPt point) {
		return this.location.distanceto(point) == 0.0;
	}

	@Override
	public Square boundingBox() {
		return new Square(this.location, 0);
	}

	@Override
	public double distanceToO() {
		return this.location.distancetoO();
	}

}
