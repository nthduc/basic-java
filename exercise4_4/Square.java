package exercise4_4;

public class Square extends AShape {
	private int size;

	public Square(CartPt location, int size) {
		super(location);
		this.size = size;
	}

	@Override
	public double area() {
		return this.size * this.size;
	}

	@Override
	public double distanceToO() {
		return this.location.distancetoO();
	}

	@Override
	public boolean contains(CartPt point) {
		int x = this.location.getX();
		int y = this.location.getY();
		return this.between(point.getX(), x, x + this.size) && this.between(point.getY(), y, y + this.size);
	}

	private boolean between(int value, int low, int high) {
		return (low <= value) && (value <= high);
	}

	@Override
	public Square boundingBox() {
		return new Square(this.location, this.size);
	}

	public boolean equals(Object obj) {
		if (null == obj || !(obj instanceof Square))
			return false;
		else {
			Square that = (Square) obj;
			return (this.location.equals(that.location)) && (this.size == that.size);
		}
	}
}
