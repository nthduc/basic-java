package exercise4_4;

public abstract class AShape implements IShape {
	protected CartPt location;

	public AShape(CartPt location) {
		this.location = location;
	}

	// tinh khoang cach tu mot diem den goc toa do O
	public double distancetoO() {
		return this.location.distancetoO();
	}

	@Override
	public abstract double area();

	public abstract boolean contains(CartPt point);

	public abstract Square boundingBox();
}
