package baigiang5_2_inven;

/**
 * Class representing a non-empty inventory.
 */
public class ConsInventory implements Inventory {
	private Toy first;
	private Inventory rest;

	/**
	 * Constructor for the ConsInventory class. Initializes the first toy and the
	 * rest of the inventory.
	 * 
	 * @param first The first toy in the inventory.
	 * @param rest  The rest of the inventory.
	 */
	public ConsInventory(Toy first, Inventory rest) {
		this.first = first;
		this.rest = rest;
	}

	/**
	 * Checks if the inventory contains a toy with the given name.
	 * 
	 * @param name The name of the toy to search for.
	 * @return Returns true if the inventory contains a toy with the given name,
	 *         false otherwise.
	 */
	public boolean contain(String name) {
		return this.first.isName(name) || this.rest.contain(name);
	}

	public int howMany() {

		return 1 + this.rest.howMany();

	}

	public Inventory raisePrice(double rate) {

		Toy aToy = this.first.copyWithRaisePrice(rate);

		return new ConsInventory(aToy, this.rest.raisePrice(rate));

	}

	public void raisePriceMutable(double rate) {

		this.first.setNewPrice(rate);

		this.rest.raisePriceMutable(rate);

	}

	public boolean equals(Object obj) {

		if (obj == null || !(obj instanceof ConsInventory))

			return false;

		else {

			ConsInventory that = (ConsInventory) obj;

			return this.first.equals(that.first)

					&& this.rest.equals(that.rest);

		}

	}
}