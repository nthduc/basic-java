package baigiang5_2_inven;

/**
 * Class representing an empty inventory.
 */
public class MTInventory implements Inventory {
	/**
	 * Checks if an empty inventory contains a toy with the given name (always
	 * returns false).
	 * 
	 * @param name The name of the toy to search for.
	 * @return Returns false since an empty inventory does not contain any toys.
	 */
	public boolean contain(String name) {
		return false;
	}

	public int howMany() {

		return 0;

	}

	public Inventory raisePrice(double rate) {

		return new MTInventory();

	}
	
	public void raisePriceMutable(double rate) { }

	public boolean equals(Object obj) {

		if (obj == null || !(obj instanceof MTInventory))

			return false;

		return true;

	}
}
