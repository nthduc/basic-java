package example;

public class Author {
	private String name;
	private int birthYear;
	public Author(String name, int birthYear){
		this.name=name;
		this.birthYear=birthYear;
	}
	
	/*
	 * Getter/Setter cua thuoc tinh
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	/*
	 * so sanh 2 tac gia co giong nhau ko
	 */
	public boolean same(Author a) {
		// TODO Auto-generated method stub
		return this.name.equals(a.getName()) && this.birthYear==a.getBirthYear();
	}
	/*
	 * Xac dinh 2 tac gia cung thoi
	 */
	public boolean sameGeneration(Author author) {
		// TODO Auto-generated method stub
		if(Math.abs(this.birthYear-author.getBirthYear())<=10)
			return true;
		else 
			return false;
	}
	
}
