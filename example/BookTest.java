package example;

import junit.framework.TestCase;

public class BookTest extends TestCase{
	/*
	 * Test ham khoi tao
	 */
	public void testConstructor(){
		 new Book("abc", 100, 2010, new Author("a",1970));
		 new Book("abc", 100, 2009, new Author("a",1970));
		 new Book("abc", 100, 2011, new Author("a",1970));
	}
	/*
	 * Test kiem tra sach duoc xuat ban nam 2010 hoac 2009
	 */
	public void testcheckCurrentBookPulished(){
		 Book b1=new Book("abc", 100, 2010, new Author("a",1970));
		 Book b2=new Book("abc", 100, 2009, new Author("a",1970));
		 Book b3=new Book("abc", 100, 2011, new Author("a",1970));
		 assertTrue(b1.checkCurrentBookPulished());
		 assertTrue(b2.checkCurrentBookPulished());
		 assertFalse(b3.checkCurrentBookPulished());
	}
	/*
	 * Test kiem tra tac gia sinh sau nam 1975
	 */
	public void testcurrentAuthorBirthYear(){
		 Book b1=new Book("abc", 100, 2010, new Author("a",1970));
		 Book b2=new Book("abc", 100, 2009, new Author("a",1977));
		 Book b3=new Book("abc", 100, 2011, new Author("a",1975));
		 assertFalse(b1.currentAuthorBirthYear());
		 assertTrue(b2.currentAuthorBirthYear());
		 assertFalse(b3.currentAuthorBirthYear());
		
	}
	/*
	 * Test kiem tra sach co phai cua tac gia hay ko?
	 */
	public void testCheckThisAuthor(){
		 Book b1=new Book("abc", 100, 2010, new Author("a",1970));
		 Book b2=new Book("abc", 100, 2009, new Author("a",1977));
		 Book b3=new Book("abc", 100, 2011, new Author("a",1975));
		 assertTrue(b1.checkThisAuthor(new Author("a",1970)));
		 assertFalse(b1.checkThisAuthor(new Author("b",1970)));
		 assertFalse(b1.checkThisAuthor(new Author("a",1971)));
	}
	/*
	 * Kiem tra cung tac gia
	 */
	public void testCheckSameAuthor(){
		 Book b1=new Book("abc", 100, 2010, new Author("a",1970));
		 Book b2=new Book("abc", 100, 2009, new Author("a",1970));
		 Book b3=new Book("abc", 100, 2011, new Author("a",1975));
		 assertTrue(b1.checkSameAuthor(b2));
		 assertFalse(b1.checkSameAuthor(b3));
		 
	}
	/*
	 * Kiem tra 2 tac gia cua 2 cuon sach cung 1 thoi (nam sinh cach nhau it hon 10nam)
	 */
	public void testCheckSameGeneration(){
		 Book b1=new Book("abc", 100, 2010, new Author("a",1970));
		 Book b2=new Book("abc", 100, 2009, new Author("a",1975));
		 Book b3=new Book("abc", 100, 2011, new Author("a",1990));
		 assertTrue(b1.checkSameGeneration(b2));
		 assertFalse(b1.checkSameGeneration(b3));
		 
	}
}
