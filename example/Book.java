package example;

/*
 * Lop dinh nghia Book theo yeu cau ...
 * 
 */
public class Book {

	String title;
	double price;
	int yearOfPublication;
	Author author;

	public Book(String title, double price,int yearOfPublication, Author author) {
		super();
		this.title = title;
		this.price = price;
		this.yearOfPublication=yearOfPublication;
		this.author = author;
	}
	/*
	 * 1. currentBook that checks whether the book was published in 2010 or 2009;
	 * this.yearOfPublication
	 * ex:
	 * new Book("abc", 100, 2010, new Author("a",1970)) => True;
	 * new Book("abc", 100, 2009, new Author("a",1970)) => True;
	 * new Book("abc", 100, 2011, new Author("a",1970)) => False;
	 */
	boolean checkCurrentBookPulished(){
		if(this.yearOfPublication == 2010 || this.yearOfPublication==2009)
			return true;
		else 
			return false;
		
	}
	/*
	 * 2. currentAuthor that determines whether a book was written by a current author (born after 1975);
	 * 
	 */
	public boolean currentAuthorBirthYear(){
		if(this.author.getBirthYear()>1975){
			return true;
		}
		else
			return false;
	}
	/*
	 * thisAuthor that determines whether a book was written by the specified author;
	 * 
	 */
	public boolean checkThisAuthor(Author a){
		if(this.author.same(a))
			return true;
		else 
			return false;
	}
	/*
	 * sameAuthor that determines whether one book was written by the same author as some other book;

	 */
	public boolean checkSameAuthor(Book b){
		if(this.author.same(b.author)){
			return true;
		}else
			return false;
	}
	/*
	 * sameGeneration that determines whether two books were written by two authors born less than 10 year apart
	 */
	public boolean checkSameGeneration(Book b){
		if(this.author.sameGeneration(b.author)){
			return true;
		}else
			return false;
	}
}
