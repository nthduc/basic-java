package exercise5_12;

public class Dot extends AShape {
	public Dot(CartPt loc) {
		super(loc);
	}

	// phuong thuc so sanh 2 diem bang nhau
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dot other = (Dot) obj;
		if (loc == null) {
			if (other.loc != null)
				return false;
		} else if (!loc.equals(other.loc))
			return false;
		return true;
	}

	// tinh dien tich cua 1 diem
	@Override
	public double area() {
		return 0.0;
	}

	// kiem tra 1 diem co trung voi diem khac hay khong
	@Override
	public boolean contains(CartPt point) {
		return this.loc.getX() == point.getX() && this.loc.getY() == point.getY();
	}

	// xac dinh hinh gioi han cua 1 diem
	@Override
	public Square boundingBox() {
		return new Square(this.loc, 0);
	}

	// tinh chu vi
	@Override
	public double perimeter() {
		return 1.0;
	}
}
