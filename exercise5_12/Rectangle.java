package exercise5_12;

public class Rectangle extends AShape {
	private int width;
	private int height;

	public Rectangle(CartPt loc, int width, int height) {
		super(loc);
		this.width = width;
		this.height = height;
	}

	// tinh dien tich hinh chu nhat
	public double area() {
		return this.width * this.height;
	}

	// kiem tra 1 diem co nam trong hcn hay ko
	@Override
	public boolean contains(CartPt point) {
		if (ShapeUtils.between(point.getX(), this.loc.getX(), this.loc.getX() + width)
				&& ShapeUtils.between(point.getY(), this.loc.getY(), this.loc.getY() + height)) {
			return true;
		} else {
			return false;
		}
	}

	// hinh gioi han cua hcn
	@Override
	public Square boundingBox() {
		if (this.height <= this.width) {
			return new Square(new CartPt(this.loc.getX(), this.loc.getY() - (this.width / 2 - this.height / 2)),
					this.width);
		} else {
			return new Square(new CartPt(this.loc.getX() - (this.height / 2 - this.width / 2), this.loc.getY()),
					this.height);
		}
	}

	// tinh chu vi hcn
	@Override
	public double perimeter() {
		return (this.width + this.height) * 2;
	}
}
