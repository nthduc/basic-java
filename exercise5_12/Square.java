package exercise5_12;

public class Square extends AShape {
	private int size;

	public Square(CartPt loc, int size) {
		super(loc);
		this.size = size;
	}

	@Override
	public String toString() {
		return "Square [size=" + size + "]";
	}

	public int getSize() {
		return size;
	}

	// xac dinh 2 hv bang nhau
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Square other = (Square) obj;
		if (loc == null) {
			if (other.loc != null)
				return false;
		} else if (!loc.equals(other.loc))
			return false;
		if (size != other.size)
			return false;
		return true;
	}

	// tinh dien tich hv
	public double area() {

		return this.size * this.size;
	}

	// xac dinh 1 diem co nam trong hinh vuong hay ko
	@Override
	public boolean contains(CartPt point) {
		if (ShapeUtils.between(point.getX(), this.loc.getX(), this.loc.getX() + this.size)
				&& ShapeUtils.between(point.getY(), this.loc.getY(), this.loc.getY() + this.size)) {
			return true;
		} else {
			return false;
		}

	}

	// hinh gioi han cua hinh vuong
	@Override
	public Square boundingBox() {
		return new Square(this.loc, this.size);
	}

	// tinh chu vi hinh vuong
	@Override
	public double perimeter() {
		return this.size * 4;
	}
}
