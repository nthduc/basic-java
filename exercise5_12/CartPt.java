package exercise5_12;

public class CartPt {
	private int x;
	private int y;

	public CartPt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "CartPt [x=" + x + ", y=" + y + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartPt other = (CartPt) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	// tinh khoang cach giua 2 diem
	public double distanceTo(CartPt that) {
		return Math.sqrt((this.x - that.x) * (this.x - that.x) + (this.y - that.y) * (this.y - that.y));
	}

	// dich diem nay sang deltalX, khoang cach deltalY
	public CartPt translate(int dX, int dY) {
		return new CartPt(this.x + dX, this.y + dY);
	}
}
