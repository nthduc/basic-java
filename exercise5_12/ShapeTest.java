package exercise5_12;

import junit.framework.TestCase;

public class ShapeTest extends TestCase{
//	Test class Cartpt
	CartPt cp1 = new CartPt(3, 4);
	CartPt cp2 = new CartPt(5, 5);
	CartPt cp3 = new CartPt(7, 9);
	
//	Test class Dot
	IShape d1 = new Dot(cp1);
	IShape d2 = new Dot(cp2);
	
//	Test class Square
	IShape s1 = new Square(cp1, 5);
	IShape s2 = new Square(cp2, 4);
	
//	Test class rectangle
	IShape r1= new Rectangle(cp1, 6, 8);
	IShape r2= new Rectangle(cp2, 6, 5);
	
//	Test class Circle
	IShape c1 = new Circle(cp1, 6);
	IShape c2 = new Circle(cp3, 5);
	
//	test class CompositeShape
	IShape cs1 = new CompositeShape(d1, s1);
	IShape cs2 = new CompositeShape(d2, s1);
	IShape cs3 = new CompositeShape(c1, s2);
	IShape cs4 = new CompositeShape(c2, r2);
	IShape cs5 = new CompositeShape(cs1, cs3);	
	
//	Test constructor
	public void testConstructor() {
//		Test class Cartpt
		CartPt cp1 = new CartPt(3, 4);
		CartPt cp2 = new CartPt(5, 5);
		CartPt cp3 = new CartPt(7, 9);
		
//		Test class Dot
		IShape d1 = new Dot(cp1);
		IShape d2 = new Dot(cp2);
		
//		Test class Square
		IShape s1 = new Square(cp1, 5);
		IShape s2 = new Square(cp2, 4);
		
//		Test class rectangle
		IShape r1= new Rectangle(cp1, 8, 6);
		IShape r2= new Rectangle(cp2, 6, 5);
		
//		Test class Circle
		IShape c1 = new Circle(cp1, 6);
		IShape c2 = new Circle(cp3, 5);
		
//		test class CompositeShape
		IShape cs1 = new CompositeShape(d1, s1);
		IShape cs2 = new CompositeShape(d2, s1);
		IShape cs3 = new CompositeShape(c1, s2);
		IShape cs4 = new CompositeShape(c2, r1);
		IShape cs5 = new CompositeShape(cs1, cs3);
		
	}
//	Test method area 
	public void testArea() {	
//		test class CompositeShape
		IShape cs1a = new CompositeShape(d1, s1);
		IShape cs2a = new CompositeShape(d2, s1);
		IShape cs3a = new CompositeShape(c1, s2);
		IShape cs4a = new CompositeShape(c2, r1);
		IShape cs5a = new CompositeShape(cs1a, cs3a);
//		Test method
		assertEquals(d1.area(), 0.0,0.001);
		assertEquals(s1.area(), 25.0,0.001);
		assertEquals(r1.area(), 48.0,0.001);
		assertEquals(c1.area(), 113.097,0.001);
		
//		Test class ComPositeShape
		assertEquals(cs1a.area(), 25.0,0.001);
		assertEquals(cs2a.area(), 25.0,0.001);
		assertEquals(cs3a.area(), 129.097,0.001);
		assertEquals(cs4a.area(), 126.54,0.001);
		assertEquals(cs5a.area(), 154.097,0.001);
	}
	
//	Test method distanceToO
	public void testDistanceToO() {
//		Test method
		assertEquals(d1.distanceToO(),5.0,0.001);
		assertEquals(s2.distanceToO(), 7.071,0.001);
		assertEquals(r1.distanceToO(), 5.0,0.001);
		assertEquals(c2.distanceToO(), 11.402,0.001);
		
		assertEquals(cs1.distanceToO(), 5.0,0.001);
		assertEquals(cs2.distanceToO(), 5.0,0.001);
		assertEquals(cs3.distanceToO(), 5.0,0.001);
		assertEquals(cs4.distanceToO(), 7.071,0.001);
		assertEquals(cs5.distanceToO(), 5.0,0.001);
	}
	
//	Test method contains
	public void testContains() {
//		Test class Cartpt
		CartPt cp1 = new CartPt(3, 4);
		CartPt cp2 = new CartPt(5, 5);
		CartPt cp3 = new CartPt(7, 10);
		
//		Test class Dot
		IShape d1 = new Dot(cp1);
		IShape d2 = new Dot(cp2);
		
//		Test class Square
		IShape s1 = new Square(cp1, 5);
		IShape s2 = new Square(cp2, 4);
		
//		Test class rectangle
		IShape r1= new Rectangle(cp1, 8, 6);
		IShape r2= new Rectangle(cp2, 6, 5);
		
//		Test class Circle
		IShape c1 = new Circle(cp1, 6);
		IShape c2 = new Circle(cp3, 5);
		
//		test class CompositeShape
		IShape cs1 = new CompositeShape(d1, s1);
		IShape cs2 = new CompositeShape(d2, s1);
		IShape cs3 = new CompositeShape(c1, s2);
		IShape cs4 = new CompositeShape(c2, r2);
		IShape cs5 = new CompositeShape(cs1, cs3);
		
//		Test method
		assertTrue(d1.contains(new CartPt(3, 4)));
		assertFalse(s1.contains(cp3));
		assertFalse(r2.contains(new CartPt(12, 5)));
		assertTrue(c1.contains(new CartPt(1, 1)));
		
		assertTrue(cs1.contains(new CartPt(3, 4)));
		assertFalse(cs2.contains(new CartPt(10, 10)));
		assertFalse(cs3.contains(new CartPt(7, 10)));
		assertTrue(cs4.contains(new CartPt(7, 10)));
		assertFalse(cs5.contains(new CartPt(7, 10)));
		assertTrue(cs5.contains(new CartPt(5, 5)));		
	
	}
//	Test method boundingBox
	public void testBoundingBox() {

//		Test method
		assertEquals(d1.boundingBox(), new Square(new CartPt(3, 4), 0));
		assertEquals(s2.boundingBox(), new Square(new CartPt(5, 5), 4));
		assertEquals(r1.boundingBox(), new Square(new CartPt(2, 4), 8));
		assertEquals(c2.boundingBox(), new Square(new CartPt(2, 4), 10));
		
		assertEquals(cs1.boundingBox(), new Square(new CartPt(3, 4),5));
		assertEquals(cs2.boundingBox(), new Square(new CartPt(3, 4), 5));
		assertEquals(cs3.boundingBox(), new Square(new CartPt(-3, -2), 12));
		assertEquals(cs4.boundingBox(), new Square(new CartPt(2, 4), 10));
		assertEquals(cs5.boundingBox(), new Square(new CartPt(-3, -2), 12));
	
	}
//	Test method perimeter
	public void testPrimeter() {

//		Test method
		assertEquals(d2.perimeter(), 1.0,0.001);
		assertEquals(s1.perimeter(), 20,0.001);
		assertEquals(r1.perimeter(), 28,0.001);
		assertEquals(c2.perimeter(), 31.416,0.001);
		
		assertEquals(cs1.perimeter(), 21.00,0.001);
		assertEquals(cs2.perimeter(), 21.00,0.001); 
		assertEquals(cs3.perimeter(), 53.7,0.001);
		assertEquals(cs4.perimeter(), 53.416,0.001);
		assertEquals(cs5.perimeter(), 74.7,0.001);
		
	
	}
}
