package exercise5_12;

public abstract class AShape implements IShape {
	protected CartPt loc;

	public AShape(CartPt loc) {
		this.loc = loc;
	}

	public abstract double area();

	//	tinh khoang cach tu hinh den goc toa do
	@Override
	public double distanceToO() {
		return this.loc.distanceTo(new CartPt(0, 0));
	}

	public abstract boolean contains(CartPt point);

	public abstract Square boundingBox();

	public abstract double perimeter();
}
