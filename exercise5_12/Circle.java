package exercise5_12;

public class Circle extends AShape {
	private int radius;

	public Circle(CartPt loc, int radius) {
		super(loc);
		this.radius = radius;
	}

	// tinh dien tich hinh tron
	@Override
	public double area() {
		return Math.PI * this.radius * this.radius;
	}

	// xac dinh 1 diem co thuoc duong tron hay khong
	@Override
	public boolean contains(CartPt cartPt) {
		if ((this.loc.distanceTo(cartPt)) * (this.loc.distanceTo(cartPt)) < this.radius * this.radius) {
			return true;
		} else
			return false;
	}

	// xac dinh hinh vuong gioi han hinh tron nay
	@Override
	public Square boundingBox() {
		return new Square(this.loc.translate(-this.radius, -this.radius), this.radius * 2);
	}

	// tinh chu vi hinh tron
	@Override
	public double perimeter() {
		return 2 * Math.PI * this.radius;
	}
}
