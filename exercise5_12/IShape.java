package exercise5_12;

public interface IShape {
	
	// tinh dien tich
	public double area();

	// tinh khoang cach tu hinh den goc toa do
	public double distanceToO();

	// kiem tra diem nam trong hinh
	public boolean contains(CartPt point);

	// hinh gioi han
	public Square boundingBox();

	// tinh chu vi
	public double perimeter();
}
