package exercise5_12;

public class CompositeShape implements IShape {
	private IShape top;
	private IShape bottom;

	public CompositeShape(IShape top, IShape bottom) {
		this.top = top;
		this.bottom = bottom;
	}

	// tinh tong chu vi
	@Override
	public double area() {
		return this.top.area() + this.bottom.area();
	}

	// tinh khoang cach nho nhat cua cac hinh
	@Override
	public double distanceToO() {
		return Math.min(this.top.distanceToO(), this.bottom.distanceToO());
	}

	// kiem tra co chua 1 diem cho truoc
	@Override
	public boolean contains(CartPt point) {
		return this.top.contains(point) || this.bottom.contains(point);
	}

	// xac dinh hinh vuong chua tat ca cac hinh
	@Override
	public Square boundingBox() {
		Rectangle re = null;
		Square bbTop = this.top.boundingBox();
		Square bbBottom = this.bottom.boundingBox();
		int x1 = Math.min(bbTop.loc.getX(), bbBottom.loc.getX());
		int y1 = Math.min(bbTop.loc.getY(), bbBottom.loc.getY());
		int x2 = Math.max(bbTop.loc.getX() + bbTop.getSize(), bbBottom.loc.getX() + bbBottom.getSize());
		int y2 = Math.max(bbTop.loc.getY() + bbTop.getSize(), bbBottom.loc.getY() + bbBottom.getSize());

		re = new Rectangle(new CartPt(x1, y1), x2 - x1, y2 - y1);

		return re.boundingBox();

	}

	// tinh tong chu vi cua cac hinh
	@Override
	public double perimeter() {
		return this.top.perimeter() + this.bottom.perimeter();
	}
}
