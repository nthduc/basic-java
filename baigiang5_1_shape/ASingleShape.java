package baigiang5_1_shape;

/**
 * Abstract class representing a single shape.
 */
public abstract class ASingleShape implements IShape {
    protected CartPt location;

    /**
     * Constructor for the ASingleShape class. Initializes the location of the shape.
     * @param location The location of the shape.
     */
    public ASingleShape(CartPt location) {
        this.location = location;
    }

    /**
     * Returns a string representation of the shape.
     * @return A string representation of the shape with its location.
     */
    public String toString() {
        return "Location: " + this.location.toString();
    }
}