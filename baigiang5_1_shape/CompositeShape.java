package baigiang5_1_shape;

/**
 * Class representing a composite shape consisting of two shapes stacked on top of each other.
 */
public class CompositeShape implements IShape {
    private IShape top;
    private IShape bottom;

    /**
     * Constructor for the CompositeShape class. Initializes the top and bottom shapes of the composite shape.
     * @param top The top shape of the composite shape.
     * @param bottom The bottom shape of the composite shape.
     */
    public CompositeShape(IShape top, IShape bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    /**
     * Returns a string representation of the composite shape.
     * @return A string representation of the composite shape with its top and bottom shapes on separate lines.
     */
    public String toString() {
        return this.top.toString() + " \n" + this.bottom.toString();
    }
}