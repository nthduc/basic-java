package baigiang5_1_shape;

/**
 * Class representing a circle with a location and radius.
 */
public class Circle extends ASingleShape {
    private int radius;

    /**
     * Constructor for the Circle class. Initializes the location and radius of the circle.
     * @param location The location of the circle.
     * @param radius The radius of the circle.
     */
    public Circle(CartPt location, int radius) {
        super(location);
        this.radius = radius;
    }
}