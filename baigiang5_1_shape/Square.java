package baigiang5_1_shape;

public class Square extends ASingleShape {

private int size;

public Square(CartPt location, int size){

super(location);

this.size = size;

}

public String toString() {

return super.toString() + "Size: " + this.size;

}

}
