package baigiang5_1_shape;

/**
 * Class representing a dot with a location.
 */
public class Dot extends ASingleShape {

    /**
     * Constructor for the Dot class. Initializes the location of the dot.
     * @param location The location of the dot.
     */
    public Dot(CartPt location) {
        super(location);
    }
}