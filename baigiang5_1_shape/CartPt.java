package baigiang5_1_shape;

/**
 * Class representing a Cartesian point with x and y coordinates.
 */
public class CartPt {
    private int x;
    private int y;

    /**
     * Constructor for the CartPt class. Initializes the x and y coordinates of the point.
     * @param x The x coordinate of the point.
     * @param y The y coordinate of the point.
     */
    public CartPt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns a string representation of the point.
     * @return A string representation of the point in the format "(x, y)".
     */
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
}