package exercise4_7;

public abstract class ATaxiVehicle {
	protected int id;
	protected int passengers;
	protected int pricePerMile;

	public ATaxiVehicle(int id, int passengers, int pricePerMile) {
		this.id = id;
		this.passengers = passengers;
		this.pricePerMile = pricePerMile;
	}

	// tinh don gia
	public abstract double fare(double mile);

	// kiem tra gia ve cho mot so mile thap hon mot luong amount hay khong
	public boolean lowerPrice(double mile, double amount) {
		return this.fare(mile) < amount;
	}

	// kiem tra gia cua phuong tien nay co re hon cua phuong tien khac trong cung so
	// mile hay khong
	public boolean cheaperThan(double mile, ATaxiVehicle that) {
		return this.fare(mile) < that.fare(mile);
	}
}
