package exercise4_7;

public class Limo extends ATaxiVehicle {
	private int minRetal;

	public Limo(int iD, int passengers, int pricePerMile, int minRetal) {
		super(iD, passengers, pricePerMile);
		this.minRetal = minRetal;
	}

	// tinh don gia
	@Override
	public double fare(double mile) {
		if (this.pricePerMile * mile < minRetal)
			return this.minRetal;
		return this.pricePerMile * mile;
	}
}
