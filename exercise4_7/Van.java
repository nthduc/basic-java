package exercise4_7;

public class Van extends ATaxiVehicle {
	private boolean access;

	public Van(int iD, int passengers, int pricePerMile, boolean access) {
		super(iD, passengers, pricePerMile);
		this.access = access;
	}

	// tinh don gia
	@Override
	public double fare(double mile) {
		return this.pricePerMile * mile + this.passengers;
	}

}
