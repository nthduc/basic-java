package exercise4_7;

import junit.framework.TestCase;

public class TaxiVehicleTest extends TestCase {
	ATaxiVehicle cab = new Cab(79, 3, 10);
	ATaxiVehicle limo = new Limo(99999, 2, 100, 500);
	ATaxiVehicle van = new Van(123, 10, 2, false);

	public void testFare() {
		assertEquals(cab.fare(10), 100.0);
		assertEquals(limo.fare(6), 600.0);
		assertEquals(limo.fare(3), 500.0);
		assertEquals(van.fare(30), 70.0);
	}

	public void testLowerPrice() {
		assertTrue(cab.lowerPrice(10.0, 200.0));
		assertTrue(limo.lowerPrice(7.0, 1000000.0));
		assertFalse(van.lowerPrice(20.0, 30.0));
	}

	public void testCheaperThan() {
		assertTrue(van.cheaperThan(10, cab));
		assertTrue(van.cheaperThan(20, limo));
		assertFalse(limo.cheaperThan(5, cab));
	}
}
