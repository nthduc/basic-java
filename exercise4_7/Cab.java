package exercise4_7;

public class Cab extends ATaxiVehicle {

	public Cab(int iD, int passengers, int pricePerMile) {
		super(iD, passengers, pricePerMile);
	}

	// tinh don gia
	@Override
	public double fare(double mile) {
		return this.pricePerMile * mile;
	}

}
