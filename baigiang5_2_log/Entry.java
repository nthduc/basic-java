package baigiang5_2_log;

public class Entry {
	private Date date;

	private double distance;

	private int duration;

	private String comment;

	
	// Constructor
	public Entry(Date date, double distance, int duration, String comment) {
		this.date = date;
		this.distance = distance;
		this.duration = duration;
		this.comment = comment;

	}
	// getDistance
	public double getDistance() {

		return this.distance;

	}
	// sameMonthInAYear
	public boolean sameMonthInAYear(int month, int year) {

		return this.date.sameMonthInAYear(month, year);

	}
	// equals
	public boolean equals(Object obj) {

		if (obj == null || !(obj instanceof Entry))

			return false;

		else {

			Entry that = (Entry) obj;

			return this.date.equals(that.date);

		}

	}
}
