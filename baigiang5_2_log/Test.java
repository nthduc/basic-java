package baigiang5_2_log;

import junit.framework.TestCase;

public class Test extends TestCase {

	public void testConstructor() {
		Entry e1 = new Entry(new Date(5, 5, 2005), 5.0, 25, "Good");

		Entry e2 = new Entry(new Date(6, 6, 2005), 3.0, 24, "Tired");

		Entry e3 = new Entry(new Date(23, 6, 2005), 26.0, 156, "Great");

		ILog l0 = new MTLog();

		ILog l1 = new ConsLog(e1, l0);

		ILog l2 = new ConsLog(e2, l1);

		ILog l3 = new ConsLog(e3, l2);

		assertEquals(l0.miles(), 0.0);

		assertEquals(l1.miles(), 5.0);

		assertEquals(l2.miles(), 8.0);

		assertEquals(l3.miles(), 34.0);

		assertEquals(l0.getLogs(6, 2005), new MTLog());

		assertEquals(l1.getLogs(6, 2005), new MTLog());

		assertEquals(l2.getLogs(6, 2005),

				new ConsLog(e2, new MTLog()));

		assertEquals(l3.getLogs(6, 2005),

				new ConsLog(e3, new ConsLog(e2, new MTLog())));
	}
}
