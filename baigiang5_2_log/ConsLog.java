package baigiang5_2_log;

public class ConsLog implements ILog {

	private Entry first;

	private ILog rest;

	public ConsLog(Entry first, ILog rest) {

		this.first = first;
		this.rest = rest;
	}

	public double miles() {

		return this.first.getDistance() +

				this.rest.miles();

	}

	public ILog getLogs(int month, int year) {

		if (this.first.sameMonthInAYear(month, year))

			return new ConsLog(this.first,

					this.rest.getLogs(month, year));

		else

			return this.rest.getLogs(month, year);

	}

	public boolean equals(Object obj) {

		if (obj == null || !(obj instanceof ConsLog))

			return false;

		else {

			ConsLog that = (ConsLog) obj;

			return this.first.equals(that.first)

					&& this.rest.equals(that.rest);

		}

	}

}
