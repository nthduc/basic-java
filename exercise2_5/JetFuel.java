package exercise2_5;

public class JetFuel {
    int quantity;
    String quality;
    int basePrice;

    /**
     * Constructor for the JetFuel class
     * @param quantity Quantity sold in integer gallons
     * @param quality Quality level as a string
     * @param basePrice Current base price of jet fuel in integer cents per gallon
     */
    public JetFuel(int quantity, String quality, int basePrice) {
        this.quantity = quantity;
        this.quality = quality;
        this.basePrice = basePrice;
    }

    /**
     * Computes the cost of the sale
     * @return the cost of the sale in integer cents
     */
    public int totalCost() {
        return this.quantity * this.basePrice;
    }

    /**
     * Computes the discounted price
     * @return the discounted price in integer cents
     */
    public int discountPrice() {
        if (this.quantity > 100000) {
            return (int) (this.totalCost() * 0.9);
        } else {
            return this.totalCost();
        }
    }
}
