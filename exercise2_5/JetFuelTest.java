package exercise2_5;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the JetFuel class using JUnit 4
 */
public class JetFuelTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorJetFuel() {
        new JetFuel(1000, "High", 100);
        new JetFuel(2000, "Medium", 200);
        new JetFuel(3000, "Low", 300);
    }

    /**
     * this is testing for totalCost function
     */
    @Test
    public void testTotalCost() {
        assertEquals(100000, new JetFuel(1000, "High", 100).totalCost());
        assertEquals(400000, new JetFuel(2000, "Medium", 200).totalCost());
        assertEquals(900000, new JetFuel(3000, "Low", 300).totalCost());
    }

    /**
     * this is testing for discountPrice function
     */
    @Test
    public void testDiscountPrice() {
        assertEquals(100000, new JetFuel(1000, "High", 100).discountPrice());
        assertEquals(400000, new JetFuel(2000, "Medium", 200).discountPrice());
        assertEquals(810000, new JetFuel(3000, "Low", 300).discountPrice());
    }
}
