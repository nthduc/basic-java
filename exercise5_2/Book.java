package exercise5_2;

/**
 * Class representing a book with a title, author, price, and year of publication.
 */
public class Book {
    private String title;
    private String author;
    private double price;
    private int year;

    /**
     * Constructor for the Book class. Initializes the title, author, price, and year of publication of the book.
     * @param title The title of the book.
     * @param author The author of the book.
     * @param price The price of the book in dollars.
     * @param year The year of publication of the book.
     */
    public Book(String title, String author, double price, int year) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.year = year;
    }

    /**
     * Returns a string representation of the book.
     * @return A string representation of the book with its fields separated by spaces.
     */
    public String toString() {
        return this.title + " " + this.price + " " + this.year + " " + this.author;
    }
}
