package exercise5_2;

/**
 * Class representing a non-empty bookstore (a list of books).
 */
public class ConsBookStore implements BookStore {
    private Book first;
    private BookStore rest;

    /**
     * Constructor for the ConsBookStore class. Initializes the first book and the rest of the list.
     * @param first The first book in the list.
     * @param rest The rest of the list.
     */
    public ConsBookStore(Book first, BookStore rest) {
        this.first = first;
        this.rest = rest;
    }

    /**
     * Returns a string representation of the bookstore (list of books).
     * @return A string representation of the list with each book on a separate line.
     */
    public String toString() {
        return this.first.toString() + "\n" + this.rest.toString();
    }
}
