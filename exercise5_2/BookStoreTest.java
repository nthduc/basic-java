package exercise5_2;

import junit.framework.TestCase;

/**
 * Class for testing the BookStore classes.
 */
public class BookStoreTest extends TestCase {
    // test constructor
    public void testConstructor() {
        // Create some books
        Book b1 = new Book("Chiec thuyen ngoai xa", "Nguyen Khoa Diem", 75.5, 1871);
        Book b2 = new Book("Tat den", "Ngo Tat To", 92.8, 1719);
        Book b3 = new Book("Song", "Xuan Quynh", 59.5, 1956);

        // Create a list with all three books
        BookStore list = new ConsBookStore(b1, new ConsBookStore(b2, new ConsBookStore(b3, new Empty())));
        // Print the list with all three books
        System.out.println(list);
    }
}