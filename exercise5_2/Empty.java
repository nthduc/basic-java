package exercise5_2;

/**
 * Class representing an empty bookstore (an empty list of books).
 */
public class Empty implements BookStore {
    /**
     * Returns a string representation of an empty bookstore (an empty string).
     * @return An empty string representing an empty bookstore.
     */
    public String toString() {
        return "";
    }
}
