package exercise4_1;

/**
 * Represents a certificate of deposit with an interest rate and a maturity date.
 */
public class CertificateOfDeposit extends BankAccount {
    private double interestRate;
    private String maturityDate;

    /**
     * Constructs a new CertificateOfDeposit object with the given id, customer name, balance, interest rate, and maturity date.
     * @param id the id of the account
     * @param customerName the name of the customer
     * @param balance the balance of the account
     * @param interestRate the interest rate for this certificate of deposit
     * @param maturityDate the maturity date for this certificate of deposit
     */
    public CertificateOfDeposit(String id, String customerName, double balance, double interestRate, String maturityDate) {
        super(id, customerName, balance);
        this.interestRate = interestRate;
        this.maturityDate = maturityDate;
    }

    /**
     * Returns the interest rate for this certificate of deposit.
     * @return the interest rate for this certificate of deposit
     */
    public double getInterestRate() {
        return interestRate;
    }

    /**
     * Returns the maturity date for this certificate of deposit.
     * @return the maturity date for this certificate of deposit
     */
    public String getMaturityDate() {
        return maturityDate;
    }
}