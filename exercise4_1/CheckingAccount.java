package exercise4_1;

/**
 * Represents a checking account with a minimum balance.
 */
public class CheckingAccount extends BankAccount {
    private double minimumBalance;

    /**
     * Constructs a new CheckingAccount object with the given id, customer name, balance, and minimum balance.
     * @param id the id of the account
     * @param customerName the name of the customer
     * @param balance the balance of the account
     * @param minimumBalance the minimum balance required for this account
     */
    public CheckingAccount(String id, String customerName, double balance, double minimumBalance) {
        super(id, customerName, balance);
        this.minimumBalance = minimumBalance;
    }

    /**
     * Returns the minimum balance required for this account.
     * @return the minimum balance required for this account
     */
    public double getMinimumBalance() {
        return minimumBalance;
    }
}
