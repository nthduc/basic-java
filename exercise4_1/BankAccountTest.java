package exercise4_1;

import junit.framework.TestCase;

public class BankAccountTest extends TestCase {
    
   // Test case for Checking Account Constructor 
   public void testCheckingAccountConstructor() {
       CheckingAccount account = new CheckingAccount("1729", "Earl Gray", 1250.0, 500.0);
       assertEquals("1729", account.getId());
       assertEquals("Earl Gray", account.getCustomerName());
       assertEquals(1250.0, account.getBalance());
       assertEquals(500.0, account.getMinimumBalance());
   }

   // Test case for Savings Account Constructor 
   public void testSavingsAccountConstructor() {
       SavingsAccount account = new SavingsAccount("2992", "Annie Proulx", 800.0, 0.035);
       assertEquals("2992", account.getId());
       assertEquals("Annie Proulx", account.getCustomerName());
       assertEquals(800.0, account.getBalance());
       assertEquals(0.035, account.getInterestRate());
   }

   // Test case for Certificate of Deposit Constructor 
   public void testCertificateOfDepositConstructor() {
       CertificateOfDeposit account = new CertificateOfDeposit("4104", "Ima Flatt", 10123.0, 0.04, "June 1, 2005");
       assertEquals("4104", account.getId());
       assertEquals("Ima Flatt", account.getCustomerName());
       assertEquals(10123.0, account.getBalance());
       assertEquals(0.04, account.getInterestRate());
       assertEquals("June 1, 2005", account.getMaturityDate());
   }
}
