package exercise4_1;

/**
 * Represents a savings account with an interest rate.
 */
public class SavingsAccount extends BankAccount {
    private double interestRate;

    /**
     * Constructs a new SavingsAccount object with the given id, customer name, balance, and interest rate.
     * @param id the id of the account
     * @param customerName the name of the customer
     * @param balance the balance of the account
     * @param interestRate the interest rate for this account
     */
    public SavingsAccount(String id, String customerName, double balance, double interestRate) {
        super(id, customerName, balance);
        this.interestRate = interestRate;
    }

    /**
     * Returns the interest rate for this account.
     * @return the interest rate for this account
     */
    public double getInterestRate() {
        return interestRate;
    }
}