package exercise4_1;

/**
 * Represents a bank account with an id number, a customer's name, and a balance.
 */
public abstract class BankAccount {
    private String id;
    private String customerName;
    private double balance;

    /**
     * Constructs a new BankAccount object with the given id, customer name, and balance.
     * @param id the id of the account
     * @param customerName the name of the customer
     * @param balance the balance of the account
     */
    public BankAccount(String id, String customerName, double balance) {
        this.id = id;
        this.customerName = customerName;
        this.balance = balance;
    }

    /**
     * Returns the id of this account.
     * @return the id of this account
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the name of the customer associated with this account.
     * @return the name of the customer associated with this account
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Returns the current balance of this account.
     * @return the current balance of this account
     */
    public double getBalance() {
        return balance;
    }
}
