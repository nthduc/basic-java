package baigiang5_1_log;

/**
 * Class representing an empty log.
 */
public class MTLog implements ILog {

    /**
     * Returns a string representation of an empty log (an empty string).
     * @return An empty string representing an empty log.
     */
    public String toString() {
        return "";
    }

    /**
     * Checks if this empty log is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this empty log is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof MTLog)) {
            return false;
        }
        return true;
    }
}