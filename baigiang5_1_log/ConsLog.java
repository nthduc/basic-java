package baigiang5_1_log;

/**
 * Class representing a non-empty log.
 */
public class ConsLog implements ILog {
    private Entry first;
    private ILog rest;

    /**
     * Constructor for the ConsLog class. Initializes the first entry and the rest of the log.
     * @param first The first entry of the log.
     * @param rest The rest of the log.
     */
    public ConsLog(Entry first, ILog rest) {
        this.first = first;
        this.rest = rest;
    }

    /**
     * Returns a string representation of the log.
     * @return A string representation of the log.
     */
    public String toString() {
        return this.first.toString() + " \n" + this.rest.toString();
    }

    /**
     * Checks if this log is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this log is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ConsLog))
            return false;
        else {
            ConsLog that = (ConsLog) obj;
            return this.first.equals(that.first)
                    && this.rest.equals(that.rest);
        }
    }
}