package baigiang5_1_log;

import junit.framework.TestCase;

public class LogTest extends TestCase {

    /**
     * Tests the constructor of the ConsLog class.
     */
    public void testConstructor() {
        // Create some entries
        Entry e1 = new Entry(new Date(5, 5, 2005), 5.0, 25, "Good");
        Entry e2 = new Entry(new Date(6, 6, 2005), 3.0, 24, "Tired");
        Entry e3 = new Entry(new Date(23, 6, 2005), 26.0, 156, "Great");

        // Create an empty log
        ILog empty = new MTLog();

        // Create a log with one entry
        ILog l1 = new ConsLog(e3, empty);

        // Create a log with two entries
        ILog l2 = new ConsLog(e2, l1);

        // Create a log with three entries
        ILog l3 = new ConsLog(e1, l2);

        // Print the log with three entries
        System.out.println(l3);

        // Create a log with all three entries using a different method
        ILog all = new ConsLog(e1, new ConsLog(e2,
                new ConsLog(e3, new MTLog())));

        // Check if the two logs are equal
        assertEquals(l3, all);
    }
}
