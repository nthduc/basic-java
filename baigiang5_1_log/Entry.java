package baigiang5_1_log;

/**
 * Class representing an entry in a log.
 */
public class Entry {
    private Date date;
    private double distance;
    private int duration;
    private String comment;

    /**
     * Constructor for the Entry class. Initializes the date, distance, duration, and comment of the entry.
     * @param date The date of the entry.
     * @param distance The distance of the entry.
     * @param duration The duration of the entry in minutes.
     * @param comment The comment of the entry.
     */
    public Entry(Date date, double distance,
                 int duration,
                 String comment) {
        this.date = date;
        this.distance = distance;
        this.duration = duration;
        this.comment = comment;
    }

    /**
     * Returns a string representation of the entry.
     * @return A string representation of the entry with its fields separated by commas.
     */
    public String toString() {
        return "date: " + this.date.toString()
                + ", distance: " + this.distance
                + ", duration: " + this.duration
                + ", comment: " + this.comment;
    }

    /**
     * Checks if this entry is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this entry is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Entry))
            return false;
        else {
            Entry that = (Entry) obj;
            return this.date.equals(that.date) &&
                    this.distance == that.distance &&
                    this.duration == that.duration &&
                    this.comment.equals(that.comment);
        }
    }
}
