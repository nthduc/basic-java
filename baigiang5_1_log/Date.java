package baigiang5_1_log;

/**
 * Class representing a date.
 */
public class Date {
    private int day;
    private int month;
    private int year;

    /**
     * Constructor for the Date class. Initializes the day, month, and year of the date.
     * @param day The day of the date.
     * @param month The month of the date.
     * @param year The year of the date.
     */
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    /**
     * Returns a string representation of the date.
     * @return A string representation of the date in the format "day/month/year".
     */
    public String toString() {
        return this.day + "/" + this.month + "/" + this.year;
    }

    /**
     * Checks if this date is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this date is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Date))
            return false;
        else {
            Date that = (Date) obj;
            return this.day == that.day &&
                    this.month == that.month &&
                    this.year == that.year;
        }
    }
}