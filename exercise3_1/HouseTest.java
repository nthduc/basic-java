package exercise3_1;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for the House class using JUnit 4
 */
public class HouseTest {
    /**
     * this is testing for Constructor
     */
    @Test
    public void testConstructorHouse() {
        new House("Ranch", 7, 375000, new Address(23, "Maple Street", "Brookline"));
        new House("Colonial", 9, 450000, new Address(5, "Joye Road", "Newton"));
        new House("Cape", 6, 235000, new Address(83, "Winslow Road", "Waltham"));
    }

    /**
     * this is testing for hasMoreRooms function
     */
    @Test
    public void testHasMoreRooms() {
        House ranch = new House("Ranch", 7, 375000, new Address(23, "Maple Street", "Brookline"));
        House colonial = new House("Colonial", 9, 450000, new Address(5, "Joye Road", "Newton"));
        House cape = new House("Cape", 6, 235000, new Address(83, "Winslow Road", "Waltham"));

        assertFalse(ranch.hasMoreRooms(colonial));
        assertTrue(colonial.hasMoreRooms(ranch));
        assertFalse(cape.hasMoreRooms(ranch));
    }

    /**
     * this is testing for inThisCity function
     */
    @Test
    public void testInThisCity() {
        House ranch = new House("Ranch", 7, 375000, new Address(23, "Maple Street", "Brookline"));
        House colonial = new House("Colonial", 9, 450000, new Address(5, "Joye Road", "Newton"));
        House cape = new House("Cape", 6, 235000, new Address(83, "Winslow Road", "Waltham"));

        assertTrue(ranch.inThisCity("Brookline"));
        assertFalse(colonial.inThisCity("Brookline"));
        assertFalse(cape.inThisCity("Brookline"));
    }

    /**
     * this is testing for sameCity function
     */
    @Test
    public void testSameCity() {
        House ranch = new House("Ranch", 7, 375000, new Address(23, "Maple Street", "Brookline"));
        House colonial = new House("Colonial", 9, 450000, new Address(5, "Joye Road", "Newton"));
        House cape = new House("Cape", 6, 235000, new Address(83, "Winslow Road", "Waltham"));

        assertFalse(ranch.sameCity(colonial));
        assertFalse(colonial.sameCity(cape));
        assertFalse(ranch.sameCity(cape));
    }
}

