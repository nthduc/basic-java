package exercise3_1;
public class House {
    String kind;
    int numberOfRooms;
    int askingPrice;
    Address address;

    /**
     * Constructor for the House class
     * @param kind Kind of the house as a string
     * @param numberOfRooms Number of rooms in the house
     * @param askingPrice Asking price of the house in dollars
     * @param address Address of the house as an Address object
     */
    public House(String kind, int numberOfRooms, int askingPrice, Address address) {
        this.kind = kind;
        this.numberOfRooms = numberOfRooms;
        this.askingPrice = askingPrice;
        this.address = address;
    }

    /**
     * Determines whether this house has more rooms than some other house
     * @param otherHouse the other house to compare with
     * @return true if this house has more rooms than the other house, false otherwise
     */
    public boolean hasMoreRooms(House otherHouse) {
        return this.numberOfRooms > otherHouse.numberOfRooms;
    }

    /**
     * Checks whether this house is in a given city
     * @param cityName the name of the city to check
     * @return true if this house is in the given city, false otherwise
     */
    public boolean inThisCity(String cityName) {
        return this.address.city.equals(cityName);
    }

    /**
     * Determines whether this house is in the same city as some other house
     * @param otherHouse the other house to compare with
     * @return true if this house is in the same city as the other house, false otherwise
     */
    public boolean sameCity(House otherHouse) {
        return this.address.city.equals(otherHouse.address.city);
    }
}