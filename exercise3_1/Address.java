package exercise3_1;

public class Address {
    int houseNumber;
    String streetName;
    String city;

    /**
     * Constructor for the Address class
     * @param houseNumber House number as an integer
     * @param streetName Street name as a string
     * @param city City name as a string
     */
    public Address(int houseNumber, String streetName, String city) {
        this.houseNumber = houseNumber;
        this.streetName = streetName;
        this.city = city;
    }
}
