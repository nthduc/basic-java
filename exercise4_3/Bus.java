package exercise4_3;

/**
 * Represents a bus with a capacity and a route.
 */
public class Bus extends Vehicle {
    private int capacity;
    private String route;

    /**
     * Constructs a new Bus object with the given id, model, capacity, and route.
     * @param id the id of the bus
     * @param model the model of the bus
     * @param capacity the number of passengers that can be carried by the bus
     * @param route the route followed by the bus
     */
    public Bus(String id, String model, int capacity, String route) {
        super(id, model);
        this.capacity = capacity;
        this.route = route;
    }

    /**
     * Returns the number of passengers that can be carried by this bus.
     * @return the number of passengers that can be carried by this bus
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Returns the route followed by this bus.
     * @return the route followed by this bus
     */
    public String getRoute() {
        return route;
    }
}
