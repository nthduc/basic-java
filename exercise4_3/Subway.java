package exercise4_3;

/**
 * Represents a subway with a capacity and a line.
 */
public class Subway extends Vehicle {
    private int capacity;
    private String line;

    /**
     * Constructs a new Subway object with the given id, model, capacity, and line.
     * @param id the id of the subway
     * @param model the model of the subway
     * @param capacity the number of passengers that can be carried by the subway
     * @param line the line served by this subway train
     */
    public Subway(String id, String model, int capacity, String line) {
        super(id, model);
        this.capacity = capacity;
        this.line = line;
    }

    /**
     * Returns the number of passengers that can be carried by this subway.
     * @return the number of passengers that can be carried by this subway
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Returns the line served by this subway train.
     * @return the line served by this subway train
     */
    public String getLine() {
        return line;
    }
}