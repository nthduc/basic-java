package exercise4_3;

/**
 * Represents a car with a capacity and a color.
 */
public class Car extends Vehicle {
    private int capacity;
    private String color;

    /**
     * Constructs a new Car object with the given id, model, capacity, and color.
     * @param id the id of the car
     * @param model the model of the car
     * @param capacity the number of passengers that can be carried by the car
     * @param color the color of the car
     */
    public Car(String id, String model, int capacity, String color) {
        super(id, model);
        this.capacity = capacity;
        this.color = color;
    }

    /**
     * Returns the number of passengers that can be carried by this car.
     * @return the number of passengers that can be carried by this car
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Returns the color of this car.
     * @return the color of this car
     */
    public String getColor() {
        return color;
    }
}

