package exercise4_3;

/**
 * Represents a limo with a capacity and a bar.
 */
public class Limo extends Vehicle {
    private int capacity;
    private boolean hasBar;

    /**
     * Constructs a new Limo object with the given id, model, capacity, and bar presence.
     * @param id the id of the limo
     * @param model the model of the limo
     * @param capacity the number of passengers that can be carried by the limo
     * @param hasBar whether or not this limo has a bar
     */
    public Limo(String id, String model, int capacity, boolean hasBar) {
        super(id, model);
        this.capacity = capacity;
        this.hasBar = hasBar;
    }

    /**
     * Returns the number of passengers that can be carried by this limo.
     * @return the number of passengers that can be carried by this limo
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Returns whether or not this limo has a bar.
     * @return true if this limo has a bar, false otherwise
     */
    public boolean hasBar() {
        return hasBar;
    }
}