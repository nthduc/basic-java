package exercise4_3;

/**
 * Represents a vehicle with an id and a model.
 */
public abstract class Vehicle {
    private String id;
    private String model;

    /**
     * Constructs a new Vehicle object with the given id and model.
     * @param id the id of the vehicle
     * @param model the model of the vehicle
     */
    public Vehicle(String id, String model) {
        this.id = id;
        this.model = model;
    }

    /**
     * Returns the id of this vehicle.
     * @return the id of this vehicle
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the model of this vehicle.
     * @return the model of this vehicle
     */
    public String getModel() {
        return model;
    }
}