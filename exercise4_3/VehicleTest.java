package exercise4_3;

import junit.framework.TestCase;

public class VehicleTest extends TestCase {
    
   // Test case for Bus Constructor 
   public void testBusConstructor() {
       Bus bus = new Bus("1234", "Blue Bird", 50, "Downtown");
       assertEquals("1234", bus.getId());
       assertEquals("Blue Bird", bus.getModel());
       assertEquals(50, bus.getCapacity());
       assertEquals("Downtown", bus.getRoute());
   }

   // Test case for Limo Constructor 
   public void testLimoConstructor() {
       Limo limo = new Limo("5678", "Lincoln", 6, true);
       assertEquals("5678", limo.getId());
       assertEquals("Lincoln", limo.getModel());
       assertEquals(6, limo.getCapacity());
       assertTrue(limo.hasBar());
   }

   // Test case for Car Constructor 
   public void testCarConstructor() {
       Car car = new Car("9101", "Toyota Camry", 5, "Red");
       assertEquals("9101", car.getId());
       assertEquals("Toyota Camry", car.getModel());
       assertEquals(5, car.getCapacity());
       assertEquals("Red", car.getColor());
   }

   // Test case for Subway Constructor 
   public void testSubwayConstructor() {
       Subway subway = new Subway("1121", "NYC Subway R160A", 240, "N");
       assertEquals("1121", subway.getId());
       assertEquals("NYC Subway R160A", subway.getModel());
       assertEquals(240, subway.getCapacity());
       assertEquals("N", subway.getLine());
   }
}

