package exercise1_1;

import junit.framework.TestCase;

/**
 * Test case for the Book class using JUnit 4
 */
public class BookTest extends TestCase {
	/**
	 * this is testing for Constructor
	 */
	public void testConstructorBook() {
		new Book("Robinson Crusoe", 15.50, 1719, "Daniel Defoe");
		new Book("Heart of Darkness", 12.80, 1902, "Joseph Conrad");
		new Book("Beach Music", 9.50, 1996, "Pat Conroy");
	}

	/**
	 * this is testing for title function
	 */
	public void testTitle() {
		assertEquals("Robinson Crusoe", new Book("Robinson Crusoe", 15.50, 1719, "Daniel Defoe").getTitle());
		assertEquals("Heart of Darkness", new Book("Heart of Darkness", 12.80, 1902, "Joseph Conrad").getTitle());
		assertEquals("Beach Music", new Book("Beach Music", 9.50, 1996, "Pat Conroy").getTitle());
	}

	/**
	 * this is testing for price function
	 */

	public void testPrice() {
		assertEquals(15.50, new Book("Robinson Crusoe", 15.50, 1719, "Daniel Defoe").getPrice(), 0.01);
		assertEquals(12.80, new Book("Heart of Darkness", 12.80, 1902, "Joseph Conrad").getPrice(), 0.01);
		assertEquals(9.50, new Book("Beach Music", 9.50, 1996, "Pat Conroy").getPrice(), 0.01);
	}
	
	/**
	 * this is testing for Year of publication of the book function
	 */
	
	public void testYearOfPublication() {
		assertEquals(1719, new Book("Robinson Crusoe", 15.50, 1719, "Daniel Defoe").getYearOfPublication());
		assertEquals(1902, new Book("Heart of Darkness", 12.80, 1902, "Joseph Conrad").getYearOfPublication());
		assertEquals(1996, new Book("Beach Music", 9.50, 1996, "Pat Conroy").getYearOfPublication());
	}
	
	/**
	 * this is testing for author of the book function
	 */
	
	public void testAuthorName() {
		assertEquals("Daniel Defoe", new Book("Robinson Crusoe", 15.50, 1719, "Daniel Defoe").getAuthorName());
		assertEquals("Joseph Conrad", new Book("Heart of Darkness", 12.80, 1902, "Joseph Conrad").getAuthorName());
		assertEquals("Pat Conroy", new Book("Beach Music", 9.50, 1996, "Pat Conroy").getAuthorName());
	}

}
