package exercise1_1;

/**
 * Book class to represent a book in a bookstore
 */
public class Book {
	/**
	 * There are 4 attributes 
	 * String title 
	 * double title 
	 * int yearOfPublication 
	 * String authorName
	 */

	String title;
	double price;
	int yearOfPublication;
	String authorName;

	/**
	 * Constructor for the Book class
	 * 
	 * @param title             Title of the book
	 * @param price             Price of the book
	 * @param yearOfPublication Year of publication of the book
	 * @param authorName        Name of the author of the book
	 */
	public Book(String title, double price, int yearOfPublication, String authorName) {
		this.title = title;
		this.price = price;
		this.yearOfPublication = yearOfPublication;
		this.authorName = authorName;
	}

	/**
	 * Returns the title of the book
	 * 
	 * @return Title of the book
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns the price of the book
	 * 
	 * @return Price of the book
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Returns the year of publication of the book
	 * 
	 * @return Year of publication of the book
	 */
	public int getYearOfPublication() {
		return yearOfPublication;
	}

	/**
	 * Returns the name of the author of the book
	 * 
	 * @return Name of the author of the book
	 */
	public String getAuthorName() {
		return authorName;
	}

}
