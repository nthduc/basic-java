package exercise5_1;

/**
 * Class representing an address with an id, street, and city.
 */
public class Address {
    private int id;
    private String street;
    private String city;

    /**
     * Constructor for the Address class. Initializes the id, street, and city of the address.
     * @param id The id of the address.
     * @param street The street of the address.
     * @param city The city of the address.
     */
    public Address(int id, String street, String city) {
        this.id = id;
        this.street = street;
        this.city = city;
    }

    /**
     * Returns a string representation of the address.
     * @return A string representation of the address with its fields separated by spaces.
     */
    public String toString() {
        return this.id + " " + this.street + " " + this.city;
    }
}