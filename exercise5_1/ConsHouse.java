package exercise5_1;

/**
 * Class representing a non-empty list of houses.
 */
public class ConsHouse implements IHouse {
    private House first;
    private IHouse rest;

    /**
     * Constructor for the ConsHouse class. Initializes the first house and the rest of the list.
     * @param first The first house in the list.
     * @param rest The rest of the list.
     */
    public ConsHouse(House first, IHouse rest) {
        this.first = first;
        this.rest = rest;
    }

    /**
     * Returns a string representation of the list of houses.
     * @return A string representation of the list with each house on a separate line.
     */
    public String toString() {
        return this.first.toString() + "\n" + this.rest.toString();
    }
}