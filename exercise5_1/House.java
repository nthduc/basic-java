package exercise5_1;

/**
 * Class representing a house with a kind, number of rooms, price, and address.
 */
public class House {
    private String kind;
    private int rooms;
    private double price;
    private Address address;

    /**
     * Constructor for the House class. Initializes the kind, number of rooms, price, and address of the house.
     * @param kind The kind of house (e.g. "Ranch", "Colonial", "Cape").
     * @param rooms The number of rooms in the house.
     * @param price The price of the house in dollars.
     * @param address The address of the house.
     */
    public House(String kind, int rooms, double price, Address address) {
        this.kind = kind;
        this.rooms = rooms;
        this.price = price;
        this.address = address;
    }

    /**
     * Returns a string representation of the house.
     * @return A string representation of the house with its fields separated by spaces.
     */
    public String toString() {
        return this.kind + " " + this.rooms + " " + this.price + " " + this.address;
    }
}
