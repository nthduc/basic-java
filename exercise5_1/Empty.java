package exercise5_1;

/**
 * Class representing an empty list of houses.
 */
public class Empty implements IHouse {
    /**
     * Returns a string representation of an empty list of houses (an empty string).
     * @return An empty string representing an empty list of houses.
     */
    public String toString() {
        return "";
    }
}
