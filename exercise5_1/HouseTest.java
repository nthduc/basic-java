package exercise5_1;

import junit.framework.TestCase;

/**
 * Class for testing the House classes.
 */
public class HouseTest extends TestCase {

    /**
     * Tests the constructor of the ConsHouse class.
     */
    public void testConstructor() {
        // Create some houses
        House h1 = new House("Ranch", 7, 375000, new Address(23, "Maple", "Brookline"));
        House h2 = new House("Colonial", 9, 450000, new Address(5, "Joye", "Newton"));
        House h3 = new House("Cape", 6, 235000, new Address(83, "Winslow", "Waltham"));

        // Create a list with all three houses
        IHouse list = new ConsHouse(h1, new ConsHouse(h2, new ConsHouse(h3, new Empty())));

        // Print the list with all three houses
        System.out.println(list);
    }
}