package baigiang5_3_ilog;

public class MTLog implements ILog {

	public double miles() {

		return 0.0;

	}

	public ILog getLogs(int month, int year) {

		return new MTLog();

	}
	
	public ILog sortByDistance() {

		return new MTLog();

		}
	
	public ILog insertInDistanceOrder(Entry e) {

		return new ConsLog(e, new MTLog());

		}

	public boolean equals(Object obj) {

		if (obj == null || !(obj instanceof MTLog))

			return false;

		return true;

	}

}
