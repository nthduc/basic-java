package baigiang5_3_ilog;

/**
 * Class representing an entry in a log with a date, distance, duration, and comment.
 */
public class Entry {
    private Date date;
    private double distance;
    private int duration;
    private String comment;

    /**
     * Constructor for the Entry class. Initializes the date, distance, duration, and comment of the entry.
     * @param date The date of the entry.
     * @param distance The distance of the entry in miles.
     * @param duration The duration of the entry in minutes.
     * @param comment The comment of the entry.
     */
    public Entry(Date date, double distance, int duration, String comment) {
        this.date = date;
        this.distance = distance;
        this.duration = duration;
        this.comment = comment;
    }

    /**
     * Returns the distance of the entry.
     * @return The distance of the entry in miles.
     */
    public double getDistance() {
        return this.distance;
    }

    /**
     * Checks if this entry is in a specific month and year.
     * @param month The month to check against this entry's date's month.
     * @param year The year to check against this entry's date's year.
     * @return Returns true if this entry is in the specified month and year, false otherwise.
     */
    public boolean sameMonthInAYear(int month, int year) {
        return this.date.sameMonthInAYear(month, year);
    }

    /**
     * Checks if this entry has a shorter or equal distance than another entry.
     * @param that The other entry to compare with.
     * @return Returns true if this entry has a shorter or equal distance than the other entry, false otherwise.
     */
    public boolean hasDistanceShorterThan(Entry that) {
        return this.distance <= that.distance;
    }

    /**
     * Checks if this entry is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this entry is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Entry))
            return false;
        else {
            Entry that = (Entry) obj;
            return this.date.equals(that.date);
        }
    }
}