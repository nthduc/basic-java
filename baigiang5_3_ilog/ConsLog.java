package baigiang5_3_ilog;

/**
 * Class representing a non-empty log of entries.
 */
public class ConsLog implements ILog {
    private Entry first;
    private ILog rest;

    /**
     * Constructor for the ConsLog class. Initializes the first entry and the rest of the log.
     * @param first The first entry in the log.
     * @param rest The rest of the log.
     */
    public ConsLog(Entry first, ILog rest) {
        this.first = first;
        this.rest = rest;
    }

    /**
     * Calculates the total distance of all entries in the log.
     * @return The total distance of all entries in the log.
     */
    public double miles() {
        return this.first.getDistance() + this.rest.miles();
    }

    /**
     * Returns a new log containing only the entries from a specific month and year.
     * @param month The month to filter by.
     * @param year The year to filter by.
     * @return A new log containing only the entries from the specified month and year.
     */
    public ILog getLogs(int month, int year) {
        if (this.first.sameMonthInAYear(month, year))
            return new ConsLog(this.first, this.rest.getLogs(month, year));
        else
            return this.rest.getLogs(month, year);
    }

    /**
     * Returns a new log sorted by distance in ascending order.
     * @return A new log sorted by distance in ascending order.
     */
    public ILog sortByDistance() {
        return this.rest.sortByDistance().insertInDistanceOrder(this.first);
    }

    /**
     * Inserts an entry into the log in ascending order by distance.
     * @param e The entry to insert into the log.
     * @return A new log with the entry inserted in ascending order by distance.
     */
    public ILog insertInDistanceOrder(Entry e) {
        if (e.hasDistanceShorterThan(this.first))
            return new ConsLog(e, this);
        else
            return new ConsLog(this.first, this.rest.insertInDistanceOrder(e));
    }

    /**
     * Checks if this log is equal to another object.
     * @param obj The object to compare with.
     * @return Returns true if this log is equal to the other object, false otherwise.
     */
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ConsLog))
            return false;
        else {
            ConsLog that = (ConsLog) obj;
            return this.first.equals(that.first) && this.rest.equals(that.rest);
        }
    }
}