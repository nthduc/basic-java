package baigiang5_3_ilog;

public interface ILog {

	// to compute the total number of miles

	// recorded in this log

	public double miles();
	public ILog getLogs(int month, int year);
	public ILog sortByDistance();
	public ILog insertInDistanceOrder(Entry e);
	}