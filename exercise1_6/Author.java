package exercise1_6;

/**
 * Author class to represent an author
 */
public class Author {
    private String name;
    private int birthYear;

    /**
     * Constructor for the Author class
     * @param name Name of the author
     * @param birthYear Birth year of the author
     */
    public Author(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    /**
     * Returns the name of the author
     * @return Name of the author
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the birth year of the author
     * @return Birth year of the author
     */
    public int getBirthYear() {
        return birthYear;
    }
}