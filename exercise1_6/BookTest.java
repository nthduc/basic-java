package exercise1_6;

import junit.framework.TestCase;

/**
 * Test case for the Book class using JUnit 4
 */
public class BookTest extends TestCase {
    /**
     * this is testing for Constructor and getter methods for Book class and Author class.
     */
    public void testConstructorAndGetters() {
        // Create an instance of an Author using its constructor.
        Author a1 = new Author("Daniel Defoe", 1660);
        
        // Test that getter methods for Author class return correct values.
        assertEquals("Daniel Defoe", a1.getName());
        assertEquals(1660, a1.getBirthYear());

        // Create an instance of a Book using its constructor.
        Book b1 = new Book("Robinson Crusoe", 15.50, 1719, a1);

        // Test that getter methods for Book class return correct values.
        assertEquals("Robinson Crusoe", b1.getTitle());
        assertEquals(15.50, b1.getPrice(), 0.01);
        assertEquals(1719, b1.getYearOfPublication());
        
        // Test that getAuthor method returns correct object.
        assertSame(a1, b1.getAuthor());
        
        // Test that getter methods for returned Author object return correct values.
        assertEquals("Daniel Defoe", b1.getAuthor().getName());
        assertEquals(1660, b1.getAuthor().getBirthYear());
        
         // Create another instance of an Author using its constructor.
         Author a2 = new Author("Joseph Conrad", 1857);
         
         // Test that getter methods for Author class return correct values.
         assertEquals("Joseph Conrad", a2.getName());
         assertEquals(1857, a2.getBirthYear());

         // Create another instance of a Book using its constructor.
         Book b2 = new Book("Heart of Darkness", 12.80, 1902, a2);

         // Test that getter methods for Book class return correct values.
         assertEquals("Heart of Darkness", b2.getTitle());
         assertEquals(12.80, b2.getPrice(), 0.01);
         assertEquals(1902, b2.getYearOfPublication());
         
         // Test that getAuthor method returns correct object.
         assertSame(a2, b2.getAuthor());
         
         // Test that getter methods for returned Author object return correct values.
         assertEquals("Joseph Conrad", b2.getAuthor().getName());
         assertEquals(1857, b2.getAuthor().getBirthYear());
         
          // Create another instance of an Author using its constructor.
          Author a3 = new Author("Pat Conroy", 1945);
          
          // Test that getter methods for Author class return correct values.
          assertEquals("Pat Conroy", a3.getName());
          assertEquals(1945, a3.getBirthYear());

          // Create another instance of a Book using its constructor.
          Book b3 = new Book("Beach Music", 9.50, 1996, a3);

          // Test that getter methods for Book class return correct values.
          assertEquals("Beach Music", b3.getTitle());
          assertEquals(9.50, b3.getPrice(), 0.01);
          assertEquals(1996, b3.getYearOfPublication());
          
          // Test that getAuthor method returns correct object.
          assertSame(a3, b3.getAuthor());
          
          // Test that getter methods for returned Author object return correct values.
          assertEquals("Pat Conroy", b3.getAuthor().getName());
          assertEquals(1945, b3.getAuthor().getBirthYear());
    }
}