package exercise1_6;

/**
 * Book class to represent a book in a bookstore
 */
public class Book {
    private String title;
    private double price;
    private int yearOfPublication;
    private Author author;

    /**
     * Constructor for the Book class
     * @param title Title of the book
     * @param price Price of the book
     * @param yearOfPublication Year of publication of the book
     * @param author Author of the book
     */
    public Book(String title, double price, int yearOfPublication, Author author) {
        this.title = title;
        this.price = price;
        this.yearOfPublication = yearOfPublication;
        this.author = author;
    }

    /**
     * Returns the title of the book
     * @return Title of the book
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the price of the book
     * @return Price of the book
     */
    public double getPrice() {
        return price;
    }

    /**
     * Returns the year of publication of the book
     * @return Year of publication of the book
     */
    public int getYearOfPublication() {
        return yearOfPublication;
    }

    /**
     * Returns the author of the book
     * @return Author of the book
     */
    public Author getAuthor() {
        return author;
    }
}
