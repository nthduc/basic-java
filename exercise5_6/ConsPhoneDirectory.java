package exercise5_6;

public class ConsPhoneDirectory implements IPhoneDirectory {
	private Directory first;
	private IPhoneDirectory rest;

	public ConsPhoneDirectory(Directory first, IPhoneDirectory rest) {
		this.first = first;
		this.rest = rest;
	}

	// tra ve ten tuong ung voi sdt nhat dinh
	@Override
	public String whoseNumber(String number) {
		if (this.first.checkNumber(number))
			return this.first.getName();
		else
			return this.rest.whoseNumber(number);

	}

	// tra ve sdt tuong ung voi ten nhat dinh
	public String phoneNumber(String name) {
		if (this.first.checkName(name))
			return this.first.getPhoneNumber();
		else
			return this.rest.phoneNumber(name);

	}

	@Override
	public String toString() {
		return this.first.toString() + "\n" + this.rest.toString();
	}

}
