package exercise5_6;

import junit.framework.TestCase;

public class PhoneDirectoryTest extends TestCase {
	Directory d1 = new Directory("Ty", "0341245785");
	Directory d2 = new Directory("Teo", "0245784139");
	Directory d3 = new Directory("Toan", "0845751248");

	IPhoneDirectory empty = new MTPhoneDirectory();
	IPhoneDirectory i1 = new ConsPhoneDirectory(d3, empty);
	IPhoneDirectory i2 = new ConsPhoneDirectory(d2, i1);
	IPhoneDirectory all = new ConsPhoneDirectory(d1, i2);

	// test constructor
	public void testConstructor() {
		System.out.println(all);
	}

	// test whoseNumber
	public void testWhoseNumber() {
		assertEquals(all.whoseNumber("0000000000"), "Not Exist");
		assertEquals(all.whoseNumber("0341245785"), "Ty");
		assertEquals(all.whoseNumber("0245784139"), "Teo");
		assertEquals(all.whoseNumber("0845751248"), "Toan");
	}

	// test phoneNumber
	public void testPhoneNumber() {
		assertEquals(all.phoneNumber("abc"), "Not Exist");
		assertEquals(all.phoneNumber("Ty"), "0341245785");
		assertEquals(all.phoneNumber("Teo"), "0245784139");
		assertEquals(all.phoneNumber("Toan"), "0845751248");
	}

}
