package exercise5_6;

public class Directory {
	private String name;
	private String phoneNumber;

	public Directory(String name, String phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
	}

	// ten
	public String getName() {
		return name;
	}

	// sdt
	public String getPhoneNumber() {
		return phoneNumber;
	}

	// kiem tra 1 ten co giong 1 ten cho truoc hay khong
	public boolean checkName(String name) {
		return this.name.equals(name);
	}

	// kiem tra 1 sdt co giong 1 sdt khac hay khong
	public boolean checkNumber(String number) {
		return this.phoneNumber.equals(number);
	}

	@Override
	public String toString() {
		return "Name: " + name + ", Phone Number: " + phoneNumber;
	}

}
