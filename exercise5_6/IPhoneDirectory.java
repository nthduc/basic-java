package exercise5_6;

public interface IPhoneDirectory {

	// tra ve ten tuong ung voi sdt nhat dinh
	public String whoseNumber(String number);

	// tra ve sdt tuong ung voi ten nhat dinh
	public String phoneNumber(String name);

}
