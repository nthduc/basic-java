package exercise5_7;

public class AnItem {
	protected String brandName;
	protected double weight;
	protected double price;

	public AnItem(String brandName, double weight, double price) {
		this.brandName = brandName;
		this.weight = weight;
		this.price = price;
	}

	// lay gia tien
	public double getPrice() {
		return this.price;
	}

	// tinh don vi gia
	public double unitPrice() {
		return this.price / this.weight;
	}

	// so sanh gia voi gia nhat dinh
	public boolean lowerPrice(double amount) {
		return this.unitPrice() < amount;

	}

	// kiem tra mat hang nay co re hon mat hang kia hay khong
	public boolean cheaperThan(AnItem that) {
		return this.unitPrice() < that.unitPrice();
	}

	// lay thuong hieu
	public String getBrandName() {
		return this.brandName;
	}

	@Override
	public String toString() {
		return brandName + " " + weight + " " + price;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof AnItem)) {
			return false;
		} else {
			AnItem that = (AnItem) obj;
			return this.brandName.equals(that.brandName) && this.weight == that.weight && this.price == that.price;
		}
	}

}
