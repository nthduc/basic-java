package exercise5_7;

public class Juice extends AnItem {
	private String flavor;
	private String packaged;

	public Juice(String brandName, double weight, double price, String flavor, String packaged) {
		super(brandName, weight, price);
		this.flavor = flavor;
		this.packaged = packaged;
	}

	@Override
	public String toString() {
		return brandName + "   " + weight + "   " + price + "   " + flavor + "   " + packaged;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Juice))
			return false;
		else {
			Juice that = (Juice) obj;
			return this.flavor.equals(that.flavor) && this.packaged.equals(that.packaged);
		}
	}

}
