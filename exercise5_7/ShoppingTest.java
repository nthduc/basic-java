package exercise5_7;

import junit.framework.TestCase;

public class ShoppingTest extends TestCase {

	AnItem i1 = new IceCream("Cream", 100.0, 110.0, "strobery", "cool");
	AnItem i2 = new Coffee("VinaCoffee", 10.0, 1000.0, "kind");
	AnItem i3 = new Juice("Orange", 30.0, 5000.0, "orange", "package");

	IShoppingList empty = new MTShoppingList();
	IShoppingList l1 = new ConsShoppingList(i1, empty);
	IShoppingList l2 = new ConsShoppingList(i2, l1);
	IShoppingList list = new ConsShoppingList(i3, l2);

	// test constructor
	public void testConstructor() {
		System.out.println("Danh sach mat hang:\n" + list);
	}

	// test howMany()
	public void testHowMany() {
		assertEquals(empty.howMany(), 0);
		assertEquals(l1.howMany(), 1);
		assertEquals(l2.howMany(), 2);
		assertEquals(list.howMany(), 3);
	}

	// test highestPrice()
	public void testHighestPrice() {
		assertEquals(empty.highestPrice(), 0.0);
		assertEquals(l1.highestPrice(), 110.0);
		assertEquals(l2.highestPrice(), 1000.0);
		assertEquals(list.highestPrice(), 5000.0);
	}

	// test brandList()
	public void testBrandList() {
		assertEquals(empty.brandList(), new MTStringList());
		assertEquals(l2.brandList(), new ConsStringList("VinaCoffee", new ConsStringList("Cream", new MTStringList())));

		assertEquals(list.brandList(), new ConsStringList("Orange",
				new ConsStringList("VinaCoffee", new ConsStringList("Cream", new MTStringList()))));
		
		System.out.println("Danh sach ten thuong hieu:\n" + list.brandList());
	}

	// test unitPrice()
	public void testUnitPrice() {
		assertEquals(i1.unitPrice(), 1.1, 0.001);
		assertEquals(i2.unitPrice(), 100.0, 0.001);
		assertEquals(i3.unitPrice(), 166.667, 0.001);
	}

	// test lowerPrice()
	public void testLowerPrice() {
		assertFalse(i1.lowerPrice(1.0));
		assertFalse(i2.lowerPrice(90.0));
		assertTrue(i3.lowerPrice(200.0));
	}

	// test cheaperThan()
	public void testCheaperThan() {
		assertTrue(i1.cheaperThan(i2));
		assertFalse(i2.cheaperThan(i1));
		assertFalse(i3.cheaperThan(i2));
	}
}
