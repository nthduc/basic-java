package exercise5_7;

public class Coffee extends AnItem {
	private String label;

	public Coffee(String brandName, double weight, double price, String label) {
		super(brandName, weight, price);
		this.label = label;
	}

	@Override
	public String toString() {
		return brandName + "   " + weight + "   " + price + "   " + label;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Coffee)) {
			return false;
		} else {
			Coffee that = (Coffee) obj;
			return this.label.equals(that.label);
		}

	}
}
