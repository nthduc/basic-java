package exercise5_7;

public interface IShoppingList {

	// tinh so luong mat hang trong danh sach
	public int howMany();

	// tra ve gia cao nhat trong danh sach
	public double highestPrice();

	// tra ve tat ca cac ten thuong hieu
	public IStringList brandList();

}
