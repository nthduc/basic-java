package exercise5_7;

public class ConsStringList implements IStringList {
	private String first;
	private IStringList rest;

	public ConsStringList(String first, IStringList rest) {
		this.first = first;
		this.rest = rest;
	}

	@Override
	public String toString() {
		return this.first + "\n" + this.rest;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ConsStringList)) {
			return false;
		} else {
			ConsStringList that = (ConsStringList) obj;
			return this.first.equals(that.first) && this.rest.equals(that.rest);
		}
	}
}
