package exercise5_7;

public class ConsShoppingList implements IShoppingList {
	private AnItem first;
	private IShoppingList rest;

	public ConsShoppingList(AnItem first, IShoppingList rest) {
		this.first = first;
		this.rest = rest;
	}

	// tra ve gia tien cao nhat
	@Override
	public double highestPrice() {
		return Math.max(this.first.getPrice(), this.rest.highestPrice());
	}

	// tra ve so luong mat hang
	@Override
	public int howMany() {
		return 1 + this.rest.howMany();
	}

	// tra ve danh sach ten thuong hieu
	@Override
	public IStringList brandList() {
		return new ConsStringList(this.first.getBrandName(), this.rest.brandList());
	}

	@Override
	public String toString() {
		return this.first.toString() + "\n" + this.rest.toString();
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ConsShoppingList)) {
			return false;
		} else {
			ConsShoppingList that = (ConsShoppingList) obj;
			return this.first.equals(that.first) && this.rest.equals(that.rest);
		}
	}

}
