package exercise5_7;

public class IceCream extends AnItem {
	private String flavor;
	private String type;

	public IceCream(String brandName, double weight, double price, String flavor, String type) {
		super(brandName, weight, price);
		this.flavor = flavor;
		this.type = type;
	}

	@Override
	public String toString() {
		return brandName + "   " + weight + "   " + price + "   " + flavor + "   " + type;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof IceCream)) {
			return false;
		} else {
			IceCream that = (IceCream) obj;
			return this.flavor.equals(that.flavor) && this.type.equals(that.type);
		}
	}
}
