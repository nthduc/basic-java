package exercise5_5;

public interface ILog {

	// tinh tong dam
	public double totalAllMiles();

	// tinh tong dam chay 1 thang
	public double totalMilesInMonth(int month, int year);

	// tinh quang duong nho nhat
	public double minDistance();

	// tinh khoang cach nho nhat dua tren khoang cach nho nhat duoc ghi lai trong
	// nhat ky
	public double minHelper(double currentMin);

}
