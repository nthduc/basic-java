package exercise5_5;

import junit.framework.TestCase;

public class EntryTest extends TestCase {
	Entry entry1 = new Entry(new Date(5, 6, 2020), 5.3, 27, "Great");
	Entry entry2 = new Entry(new Date(6, 6, 2020), 2.8, 24, "Tired");
	Entry entry3 = new Entry(new Date(23, 6, 2020), 26.2, 159, "Exhausted");
	Entry entry4 = new Entry(new Date(6, 7, 2020), 8.8, 23, "Tired");

	ILog empty = new MTLog();
	ILog ie1 = new ConsLog(entry1, empty);
	ILog ie2 = new ConsLog(entry2, ie1);
	ILog ie3 = new ConsLog(entry3, ie2);
	ILog all = new ConsLog(entry4, ie3);

	public void testConstructor() {
		System.out.println(all);
	}

	// test tong dam
	public void testTotalAllMiles() {
		assertEquals(empty.totalAllMiles(), 0, 0.001);
		assertEquals(ie1.totalAllMiles(), 5.3, 0.001);
		assertEquals(ie2.totalAllMiles(), 8.1, 0.001);
		assertEquals(ie3.totalAllMiles(), 34.3, 0.001);
		assertEquals(all.totalAllMiles(), 43.1, 0.001);

	}

	// test tong dam chay 1 thang
	public void testTotalMilesInMonth() {
		assertEquals(all.totalMilesInMonth(6, 2020), 34.3, 0.001);
		assertEquals(all.totalMilesInMonth(7, 2020), 8.8, 0.001);
		assertEquals(all.totalMilesInMonth(9, 2020), 0.0, 0.001);
	}

	// test quang duong nho nhat
	public void testMinDistance() {
		assertEquals(empty.minDistance(), 0, 0.001);
		assertEquals(ie2.minDistance(), 2.8, 0.001);
		assertEquals(all.minDistance(), 2.8, 0.001);

	}
}
