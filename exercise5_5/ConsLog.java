package exercise5_5;

public class ConsLog implements ILog {
	private Entry first;
	private ILog rest;

	public ConsLog(Entry first, ILog rest) {
		this.first = first;
		this.rest = rest;
	}

	// tinh tong dam da chay
	@Override
	public double totalAllMiles() {
		return this.first.getDistance() + this.rest.totalAllMiles();
	}

	// tinh tong dam chay trong 1 thang
	@Override
	public double totalMilesInMonth(int month, int year) {
		return this.first.getDistance(month, year) + this.rest.totalMilesInMonth(month, year);
	}

	// tinh quang duong nho nhat
	public double minDistance() {
		return minHelper(this.first.getDistance());
	}

	// tinh khoang cach nho nhat dua tren khoang cach nho nhat duoc ghi lai trong
	// nhat ky
	public double minHelper(double currentMin) {
		if (this.first.getDistance() < currentMin)
			return this.rest.minHelper(this.first.getDistance());
		else
			return this.rest.minHelper(currentMin);

	}

	@Override
	public String toString() {
		return this.first.toString() + "\n" + this.rest.toString();
	}

}
