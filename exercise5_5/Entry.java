package exercise5_5;

public class Entry {
	private Date date;
	private double distance;
	private int duration;
	private String comment;

	public Entry(Date date, double distance, int duration, String comment) {
		this.date = date;
		this.distance = distance;
		this.duration = duration;
		this.comment = comment;
	}

	// kiem tra thang co trung nhau ko
	public boolean sameMonthInAYear(int month, int year) {
		return this.date.checkMonth(month, year);

	}

	// tinh khoang cach chay 1 ngay
	public double getDistance(int month, int year) {
		if (this.date.checkMonth(month, year)) {
			return this.distance;
		}
		return 0;
	}

	// khoang cach
	public double getDistance() {
		return distance;
	}

	@Override
	public String toString() {
		return "Date: " + date + "\nDistance: " + distance + "\nDuration: " + duration + "\nComment: " + comment + "\n";
	}

}
