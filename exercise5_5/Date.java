package exercise5_5;

public class Date {
	private int day;
	private int month;
	private int year;

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	// kiem tra 2 thang co cung nhau ko
	public boolean checkMonth(int month2, int year2) {
		if (this.month == month2 && this.year == year2) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return day + "/" + month + "/" + year;
	}

}
