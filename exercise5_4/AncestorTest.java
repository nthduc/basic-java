package exercise5_4;

import junit.framework.TestCase;

public class AncestorTest extends TestCase {
	
	public void testAncestor() {
		IAncestorTree unknown = new Unknown();
		IAncestorTree ty = new ConsAncesterTree(new Person("Nguyen Van Ty", 1988), unknown, unknown);
		IAncestorTree teo = new ConsAncesterTree(new Person("Nguyen Van Teo", 1993), unknown, unknown);
		IAncestorTree cuoi = new ConsAncesterTree(new Person("Nguyen Van Cuoi", 1990), unknown, unknown);
		
		IAncestorTree hoa = new ConsAncesterTree(new Person("Nguyen Thi Hoa", 1992), unknown, unknown);
		IAncestorTree lan = new ConsAncesterTree(new Person("Nguyen Thi Lan", 1998), unknown, unknown);
		IAncestorTree nga = new ConsAncesterTree(new Person("Nguyen Thi Nga", 1990), unknown, unknown);
		
		IAncestorTree hoang = new ConsAncesterTree(new Person("Nguyen Van Hoang", 2002), ty, hoa);
		IAncestorTree nam = new ConsAncesterTree(new Person("Nguyen Hoang Nam", 2000), teo, lan);
		IAncestorTree thien = new ConsAncesterTree(new Person("Nguyen Van Thien", 2001), cuoi, nga);
		
		System.out.println(hoang);
	}
}
