package exercise5_4;

public class Person {
	private String name;
	private int birthYear;

	public Person(String name, int birthYear) {
		this.name = name;
		this.birthYear = birthYear;
	}

	@Override
	public String toString() {
		return this.name + " " + this.birthYear;
	}
}
